import { Army } from "../mapObjects/army";
import { Card } from "./card";
import { helper } from "../helper";
import { Objective } from "../objectives/objective";
import { MapObject } from "./mapObject";

export class ObjStatus {
	progress: number = 0;
	status: ExpeditionStatus = ExpeditionStatus.Pending;
	countDown: number = 0;

	get failed(): boolean {
		return this.status == ExpeditionStatus.Failed;
	}

	get passed(): boolean {
		return this.status == ExpeditionStatus.Completed;
	}
}

export class Expedition extends MapObject {
	army: Army = null;
	cards: Card[] = [];
	target: MapObject = null;
	total: number = 0;
	curStatus: ObjStatus = null;
	curObj: Objective = null;
	damageTaken: number = 0;
	travelRemaing = 0;
	success: boolean = false;
	absorb: number = 0;
	lucky: boolean = false;
	destroyed:boolean = false;
	overtake:number = 0;
	overtaken:boolean = false;
	started:boolean = false;
	eta: number = 0;
	damageDelt: number = 0;

	constructor(army: Army, target: MapObject) {
		super(ObjType.Expedition, army.game)
		this.army = army;
		this.team = army.team;
		this.target = target;
	}

	addCard(unit: Card) {
		this.cards.push(unit);
		this.onCardsChanged();
	}

	removeCard(unit: Card) {
		helper.removeFromArray(unit, this.cards);
		this.onCardsChanged();
	}

	validate(target: MapObject): boolean {
		this.onCardsChanged();

		if (target != this && target != null) {
			let validTiles = target.tile.adjacent.filter(t => t.occupant == null);
			if (validTiles.length == 0) {
				//this.team.inform('Expedition has no open path to mission.');
				return false;
			}
		}

		var alreadyInExp:boolean = false;

		this.cards.forEach(c => {
			if(c.expedition){
				alreadyInExp = true;
			}
		});

		if (alreadyInExp) {
			return false;
		}
		else if (this.hp <= 0) {
			return false;
		}
		
		return true;
	}

	onCardsChanged() {
		let skills = this.skills;
		skills.reset();

		var hp = 0;

		this.cards.forEach(u => {
			hp += u.hp;
			for (let prop in u.skills) {
				let type = <SkillType>prop;
				let cur = this.getSkill(type);
				this.setSkill(type, cur + u.def.skills[prop]);
			}
		});

		this.hp = hp;

		if (this.target) {
			this.eta = this.calcETA();
		} else {
			this.eta = 0;
		}
	}

	start(target: MapObject) {
		let t = target == this ? this.army.tile.getRandomNeighbor(true) : target.tile.getRandomNeighbor(true);
		if (t == null) {
			this.destroy();
			return;
		}

		if (!this.validate(target)) {
			this.team.inform('Invalid Mission', true);
			this.destroy();
			return
		}

		this.setTile(t);
		this.army.expeditions.push(this);
		this.army.tileMap.exposeTile(t);
		this.army.tileMap.exposeTile(target.tile);

		this.cards.forEach(c => {
			c.expedition = this;
		});
		
		this.army.calcHP();

		if (this.hp <= 0) {
			throw new Error('Expedition has no units');
		}

		var tt = this.army.tile.getDistanceFrom(t);
		if (tt > 0 && this.tryUseItem(ItemType.Teleport)) {
			tt = 0;
		}
		this.travelRemaing = tt;

		while (this.tryUseItem(ItemType.Shield)) {
			this.absorb += 5;
		}

		this.lucky = this.tryUseItem(ItemType.LuckyDie);

		this.setNextObjective();
		target.engage(this);
		this.started = true;
	}

	setNextObjective() {
		var objectives = this.target.objectives;
		var lastObj = this.curObj;
		this.curObj = null;

		let nextIdx = 0;
		if(lastObj != null){
			nextIdx = objectives.indexOf(lastObj);
			nextIdx++;
		}

		for(let i = nextIdx; i<objectives.length; i++){
			let obj = objectives[i];
			this.curStatus = new ObjStatus();
			obj.setStatus(this, this.curStatus);
			if(!this.curStatus.passed){
				this.curObj = obj;
				break;
			}
		}
	}

	calcETA(): number {
		let objectives = this.target.objectives;
		var result = 0;

		if (this.started) {
			result += this.travelRemaing;
		} else {
			var tt = this.army.getDistanceFrom(this.target);
			if (this.hasItem(ItemType.Teleport)) {
				tt = 0;
			}

			result += tt;
		}

		let startIdx = 0;
		if (this.curObj != null) {
			startIdx = objectives.indexOf(this.curObj) + 1;
			result += this.curStatus.countDown;
		}

		for (let i = startIdx; i < objectives.length; i++) {
			let objective = objectives[i];

			let tasks = [TaskType.GoldReq, TaskType.Requirement, TaskType.Chance, TaskType.Trap]
			if (tasks.indexOf(objective.type) >= 0) {
				result += 1;
			}
			else {
				var amount = objective.amount;
				if (objective.skill) {
					amount -= this.getSkill(objective.skill);
				}

				result += Math.max(amount, 1);
			}
		}

		return result;
	}

	hasItem(type:ItemType){
		return this.cards.find(c => { return c.name == type }) != null;
	}

	tryUseItem(type: ItemType) {
		var card = this.cards.find(c => { return c.name == type });
		if (card) {
			card.destroy();
			return true;
		}

		return false;
	}

	takeDamage(damage: number) {
		if (this.absorb > 0) {
			var amount = (this.absorb < damage) ? this.absorb : damage;
			this.absorb -= amount;
			damage -= amount;
		}

		super.takeDamage(damage);

		var units = this.cards.filter(c => c.def.isUnit);
		let unitHP = 0;
		units.forEach(u => unitHP += u.hp);
		var damage = unitHP - this.hp;

		while (damage > 0 && units.length > 0) {
			var unit = helper.getRandomItem(units);
			if (unit.hp <= damage) {
				unit.destroy();
				helper.removeFromArray(unit, units);
				damage -= unit.hp;
				this.team.inform('Unit lost: ' + unit.name);
			} else {
				break;
			}
		}
	}

	startTurn() {
		if (this.travelRemaing > 0) {
			this.travelRemaing--;
		} else if (!this.target.destroyed) {
			if (this.target.hp > 0 && this.target != this) {
				this.battle();
			} else {
				this.updateExp();
			}
		}

		if (this.target.destroyed) {
			this.destroy();
		} else if (!this.destroyed) {
			this.eta = this.calcETA();
			this.changed = true;
		}
	}

	battle() {
		let skill = this.getSkill(SkillType.Combat);
		let dmg = helper.getRandomInt(1, 1 + skill);
		this.damageDelt += dmg;
		this.target.takeDamage(dmg);

		if (this.target.hp <= 0 && this.objectives.length == 0) {
			this.success = true;
		}

		skill = this.target.getSkill(SkillType.Combat);
		dmg = helper.getRandomInt(1, 1 + skill);
		this.takeDamage(dmg);
	}

	updateExp() {
		var objStatus = this.curStatus;

		if (this.curObj) {
			this.curObj.startTurn(this, objStatus);

			if (objStatus.failed) {
				this.destroy();
			}
			else if (objStatus.passed) {
				this.setNextObjective();
			}
		}

		if (this.curObj == null) {
			this.success = true;
			this.target.complete(this);
		}
	}

	toJSON():IMapObject{
		let result = super.toJSON();
		result.eta = this.eta;
		result.travelRemaing = this.travelRemaing;
		return result;
	}

	destroy() {
		super.destroy();

		this.cards.forEach(m => {
			m.expedition = null;
		});

		this.army.calcHP();

		helper.removeFromArray(this, this.army.expeditions);
		
		if (this.success) {
			let reward = this.target.reward;
			if (!reward && this.damageDelt) {
				reward = {
					type: RewardType.Score,
					amount: this.damageDelt
				}
			}

			if (reward) {
				if (reward.type == RewardType.Gold && this.tryUseItem(ItemType.Relic)) {
					reward.amount *= 2;
				}
				this.team.recieveRewards([reward]);
			}
		} else {
			this.team.inform('Mission Failed');
		}
		this.target.removeEngagement(this);
	}
}