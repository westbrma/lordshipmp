interface ISkillLevel {
	skill: SkillType;
	count: number;
	def: ISkillDef;
}

export class Skills {
	items: ISkillLevel[] = [];
	map: SkillLevelMap = {};

	set() {
		this.items.length = 0;
		for (let prop in this.map) {
			this.items.push({
				skill: <SkillType>prop,
				count: this.map[prop],
				def: skillMap[prop]
			});
		}

		this.items.sort((a, b) => {
			var x = a.skill;
			var y = b.skill;
			return x < y ? -1 : x > y ? 1 : 0;
		});
	}

	reset() {
		this.items = [];
		this.map = {};
	}
}