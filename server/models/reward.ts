import { Card } from "./card";

export class Reward implements IReward {
	type: RewardType;
	amount?: number;
	item?: Card;
}