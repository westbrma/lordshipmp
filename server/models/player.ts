import { Game } from "./game";
import { Client } from "./client";
import { PlayerArmy } from "../mapObjects/playerArmy";

export class Player implements IPlayer{
	id:number;
	name:string;
	color:string;
	army:PlayerArmy;
	client:Client;

	constructor(client:Client, p:IPlayer){
		this.id = client.id;
		this.client = client;
		this.name = p.name;
		this.color = p.color;
	}

	toJSON():IPlayer{
		return {
			id:this.id,
			name:this.name,
			color:this.color
		}
	}
}