import { helper } from "../helper";

class CityNames {
	free: string[];
	list: string[] = [
		'Braifgend',
		'Khuakwood',
		'Yiemgate',
		'Atrifast',
		'Ecruustead',
		'Evlock',
		'Flane',
		'Deah',
		'Keunury',
		'Istriprison',
		'Bougas',
		'Ceridge',
		'Duemery',
		'Oslecshire',
		'Usheychester',
		'Clery',
		'Brolk',
		'Fleigh',
		'Praphester',
		'Grieglord',
		'Shotnard',
		'Kroamery',
		'Shishire',
		'Iglapvale',
		'Azepwell',
		'Podon',
		'Glario',
		'Leah',
		'Chelouver',
		'Vokarc',
		'Woxhall',
		'Tappen',
		'Lincolnshire',
		'Fuquay',
		'Beaver',
		'Big Flats',
		'Tarentum',
		'Grover',
		'Dolton',
		'Makawao',
		'Hayward',
		'Plum Branch',
		'Belden',
		'Dysart',
		'Oswayo',
		'Northville',
		'Gallina',
		'Tilton Northfield',
		'Diamond Bluff',
		'Elvaston',
		'Munjor',
		'Carter Lake',
		'Little Falls',
		'Zap',
		'Tallulah Falls',
		'Bar Harbor',
		'Seconsett Island',
		'Chadbourn',
		'Tuscaloosa',
		'Marinette'
	];

	constructor() {
		this.reset();
	}

	reset() {
		this.free = this.list.slice();
		helper.shuffleArray(this.free);
	}

	getName() {
		return this.free.pop();
	}
}

export let cityNames = new CityNames();