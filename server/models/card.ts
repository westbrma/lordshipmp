import { Army } from "../mapObjects/army";
import { Expedition } from "./expedition";
import { helper } from "../helper";

export class Card {
	id:number;
	selected:boolean = false;
	expedition:Expedition = null;
	army:Army = null;
	def:ICardDef = null;

	constructor(def:ICardDef){
		this.id = helper.genID();
		this.def = def;
	}

	get name(){
		return this.def.name;
	}

	get info():string{
		if(this.def.info){
			return this.def.name + ': ' + this.def.info;
		}else{
			return this.def.name;
		}
	}

	get skills():SkillLevelMap{
		return this.def.skills;
	}

	get hp(){
		if(!this.def.isUnit) return 0;
		return 3;
	}

	getSkill(type:SkillType){
		return (this.skills && this.skills[type]) ? this.skills[type] : 0;
	}

	toJSON():ICard {
		return {
			id:this.id,
			name:this.name,
			onMission: this.expedition != null
		}
	}

	destroy(){
		if(this.army){
			this.army.removeCard(this);
		}

		if(this.expedition){
			this.expedition.removeCard(this);
		}
	}
}

export class Unit extends Card{
	type:UnitType;

	constructor(type:UnitType) {
		super(unitMap[type]);
		this.type = type;
	}
}