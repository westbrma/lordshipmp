import { Tile } from "./tile";
import { MapObject } from "./mapObject";
import { Unit } from "./card";
import { Army } from "../mapObjects/army";
import { helper } from "../helper";
import { cityNames } from "./cityNames";
import { Game } from "./game";

export class City extends MapObject {
	tiles: Tile[] = [];
	expPts: number = 0;
	expProgress: number = 0;
	level:number = 0;

	prodAmount = 0;
	activeProd:IProdDef = prodMap[ProdType.Castle];

	production: number = 0;
	food: number = 0;
	gold: number = 0;
	population: number = 0;

	name: string = null;
	units: Unit[] = [];
	upgrades: { [key: string]: number } = {};
	_score = 0;
	_ttCityUpdate: number = 1;

	constructor(army: Army, game:Game, name?: string) {
		super(ObjType.City, game);
		this.team = army.team;
		this.team.cities.push(this);

		if (name) {
			this.name = name
		} else {
			this.name = cityNames.getName();
		}

		var tile = army.tile;
		army.setTile(army.tile.getRandomNeighbor());
		this.setTile(tile);
		this._controlTiles();

		this._heal();
	}

	die() {
		this.destroy();
	}

	startTurn() {
		super.startTurn();
		this._ttCityUpdate++;
		while (this._ttCityUpdate >= CityTurns) {
			this._doCityTurn();
			this._ttCityUpdate = 0;
			this.changed = true;
		}
	}

	_doCityTurn() {
		var expPts = Math.floor(this.food / 10);
		if (expPts == 0) {
			var expPts = 1;
		}

		this.expPts += expPts;
		this.team.gold += this.gold;
		this.team.score += Math.floor(this.population / 10);

		this.prodAmount += this.production;
		if(this.prodAmount >= this.activeProd.cost){
			this.prodAmount -= this.activeProd.cost;
			if(this.activeProd.name == ProdType.Knight){
				this.team.addCard(new Unit(UnitType.Knight));
			}else if(this.activeProd.name == ProdType.Castle){
				this.level++;
				this.setSkill(SkillType.Combat, this.level);
			}
		}
		
		if (!this.underAttack) {
			this._heal();
		}
	}

	_heal(){
		this.hp = 10 + (this.level * 5);
	}

	get underAttack(): boolean {
		return this.engagements.length > 0;
	}

	exposeTiles() {
		this.tiles.forEach(t => {
			this.team.tileMap.exposeTiles(t, CityView);
		});
	}

	destroy() {
		super.destroy();
		for (var i = 0; i < this.tiles.length; i++) {
			var tile = this.tiles[i];
			tile.setControlledBy(null);
		}

		helper.removeFromArray(this, this.team.cities);
	}

	static canBuild(tile: Tile): boolean {
		if (tile.city == null && [TileType.Grass, TileType.Sand, TileType.Dirt, TileType.Trees, TileType.Mountain].indexOf(tile.type) >= 0) {
			return true;
		}

		return false;
	}

	canExpand(tile: Tile): boolean {
		if (tile.city != null) {
			if (this.expPts < 2
				|| tile.city.team == this.team
				|| tile.occupant == tile.city) {
				return false;
			}
		}
		else if (this.expPts < 1) {
			return false
		}

		var adj = false;
		tile.adjacent.forEach(t => {
			if (t.city == this) {
				adj = true;
			}
		});
		return adj;
	}

	expand(tile: Tile) {
		if (!this.canExpand(tile)) {
			this.team.inform('Invalid expand tile', true);
			return;
		}

		this.expPts -= tile.city ? 2 : 1;
		this.changed = true;
		this._controlTile(tile);
	}

	_controlTiles() {
		var radius = 1;
		for (var x = -radius; x <= radius; x++) {
			for (var y = -radius; y <= radius; y++) {
				var tile = this.game.board.tile(x + this.tile.x, y + this.tile.y);
				if (tile != null && tile.team == null) {
					this._controlTile(tile);
				}
			}
		}
	}

	_controlTile(tile: Tile) {
		this.tiles.push(tile);
		tile.setControlledBy(this);
		this.team.tileMap.exposeTiles(tile, CityView);
		var def = tileDefs.getDef(tile.type);
		this.population += def.Population;
		this.production += def.Production;
		this.food += def.Food;
		this.gold += def.Gold;
	}

	toJSON(mine?:boolean):ICity{
		var result:ICity = <ICity>super.toJSON();
		result.name = this.name;
		if(mine){
			result.expPts = this.expPts;
			result.population = this.population;
			result.production = this.production;
			result.food = this.food;
			result.gold = this.gold;
			result.prod = this.activeProd.name;
			result.prodAmount = this.prodAmount;
		}
		
		return result;
	}
}