import { City } from "./city";
import { MapObject } from "./mapObject";
import { Army } from "../mapObjects/army";
import { Board } from "./board";
import { helper } from "../helper";

export class Tile {
	x: number;
	y: number;
	occupant: MapObject = null;
	type: TileType;
	city: City = null;
	_adj: Tile[] = null;
	board:Board = null;

	constructor(x: number, y: number, type: TileType, board:Board) {
		this.x = x;
		this.y = y;
		this.type = type;
		this.board = board;
	}

	get coord():ICoord{
		return {
			x:this.x,
			y:this.y
		}
	}

	get team():Army{
		return this.city ? this.city.team : null;
	}

	get west() {
		return this.board.tile(this.x - 1, this.y);
	}

	get east() {
		return this.board.tile(this.x + 1, this.y);
	}

	get north() {
		return this.board.tile(this.x, this.y - 1);
	}

	get south() {
		return this.board.tile(this.x, this.y + 1);
	}

	get adjacent() {
		if (this._adj == null) {
			var adj = [];
			if (this.west) adj.push(this.west);
			if (this.east) adj.push(this.east);
			if (this.south) adj.push(this.south);
			if (this.north) adj.push(this.north);
			this._adj = adj;
		}

		return this._adj;
	}

	isAdjacent(other: Tile): boolean {
		return this.adjacent.indexOf(other) >= 0;
	}

	getRandomNeighbor(open: boolean = true, notControlled: boolean = false): Tile {
		var adj = this.adjacent.slice(0);
		while (adj.length > 0) {
			var idx = helper.getRandomInt(0, adj.length - 1);
			var tile = adj[idx];
			if ((!open || tile.occupant == null) && (!notControlled || tile.city == null)) {
				return tile;
			} else {
				adj.splice(idx, 1);
			}
		}

		return null;
	}

	setControlledBy(city: City) {
		this.city = city;
		this.board.game.tileChanges.push(this);
	}

	get landTile(): boolean {
		return this.def.Type != TileType.Water;
	}

	getRawDistanceFrom(fromTile: Tile): number {
		let xDist = Math.abs(this.x - fromTile.x);
		let yDist = Math.abs(this.y - fromTile.y);
		return xDist + yDist;
	}

	getDistanceFrom(fromTile: Tile): number {
		let xDist = Math.abs(this.x - fromTile.x);
		let yDist = Math.abs(this.y - fromTile.y);
		var tt = xDist + yDist;
		return Math.ceil(tt/MaxMove);
	}

	get def(): TileDef {
		return tileDefs.getDef(this.type);
	}
}