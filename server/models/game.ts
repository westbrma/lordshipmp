import { helper } from '../helper';
import { Player } from './player';
import { socket } from '../gameSocket';
import { Board } from './board';
import { MapObject } from './mapObject';
import { Army } from '../mapObjects/army';
import { UpdateGame } from '../messages/updateGame';
import { PlayerArmy } from '../mapObjects/playerArmy';
import { Client } from './client';
import { Tile } from './tile';
import { AIArmy } from '../mapObjects/aiArmy';
import { ObjGenerator } from '../services/objGenerator';

export class Game implements IGame {
	id:number;
	active:boolean = false;
	players:Player[] = [];
	armies:Army[] = [];
	maxPlayers:number;
	topScore:number = 100;
	ownerId: number;
	newObjects:MapObject[] = [];
	deletedObjs:number[] = [];
	mapObjects: MapObject[] = [];
	mapObjectMap: Map<number, MapObject> = new Map();
	board:Board;
	completedTurns = 0;
	tileChanges:Tile[] = [];
	round:number = 0;
	generator:ObjGenerator;

	constructor(){
		this.generator = new ObjGenerator(this);
	}
	
	addPlayer(client:Client, msg:IJoinGameMsg){
		client.player = new Player(client, msg.player);
		this.players.push(client.player);
		client.game = this;
		
		msg.joined = true;
		msg.player = client.player;
		socket.sendMessage(client, msg);

		this.boardcastInfo();
		socket.broadcastPost(`${client.player.name} joined the game.`, this);
	}

	addObject(obj: MapObject) {
		this.newObjects.push(obj);
		this.generator.addObject(obj, 1);
		this.mapObjects.push(obj);
		this.mapObjectMap.set(obj.id, obj);
		if(this.mapObjectMap.size != this.mapObjects.length){
			throw new Error('something bad happened');
		}
	}

	removeObject(obj: MapObject) {
		this.mapObjectMap.delete(obj.id);
		helper.removeFromArray(obj, this.mapObjects);
		this.deletedObjs.push(obj.id);
		helper.removeFromArray(obj, this.newObjects);
		this.generator.removeObject(obj);
		if(this.mapObjectMap.size != this.mapObjects.length){
			throw new Error('something bad happened');
		}
	}
	
	boardcastInfo(){
		var info:IGameInfo = {
			type:MessageType.GameInfo,
			game: this.toJSON(true)
		}

		socket.broadcast(info, this);
	}

	toJSON(showPlayers:boolean):IGame{
		var result:IGame = {
			id: this.id,
			active: this.active,
			maxPlayers: this.maxPlayers,
			ownerId: this.ownerId,
			players: []
		};

		if(showPlayers){
			this.players.forEach(p => {
				result.players.push(p.toJSON());
			})
		}

		return result;
	}

	removePlayer(player:Player){
		helper.removeFromArray(player, this.players);

		if(this.players.length == 0 || player.id == this.ownerId){
			socket.destroyGame(this);
		}else{
			socket.broadcastPost(`${player.name} left the game.`, this);
			this.boardcastInfo();
		}
	}

	takeTurn(player:Player, turn:ITakeTurn){
		player.army.turn = turn;
		this.completedTurns++;
		if(this.completedTurns == this.players.length){
			this.processRound();
		}
	}

	processRound(){
		var update = new UpdateGame();

		this.mapObjects.forEach(o => {
			o.startTurn();
		});

		this.newObjects.forEach(o => {
			o.startTurn();
		});

		if(this.round % 5 == 0){
			this.generator.createObjects();
		}

		this.completedTurns = 0;
		this.round++;
		this.sendUpdate(update);
	}

	sendUpdate(update:UpdateGame){
		this.mapObjects.forEach(o => {
			if(!o.destroyed && o.changed){
				update.mapObjects.push(o);
				o.changed = false;
			}
		});
		
		update.deletedIds = this.deletedObjs.slice(0);

		this.tileChanges.forEach(t => {
			update.tileUpdates.push(t);
		});

		this.deletedObjs = [];
		this.tileChanges = [];
		this.newObjects = [];

		this.players.forEach(p => {
			let msg = update.toJSON(p.army);
			socket.sendMessage(p.client, msg);
			p.army.informs = [];
		});
	}

	start(gameInfo:IStartGame){
		this.topScore = gameInfo.victoryPoints;
		this.board = new Board(gameInfo.mapWidth, gameInfo.mapHeight, this);

		this.players.forEach(p => {
			p.army = new PlayerArmy(this, p);
			this.armies.push(p.army);
			p.army.setTile(this.board.randomTile());
		});

		for(let i=0; i<gameInfo.aiPlayers; i++){
			let ai = new AIArmy(this);
			ai.setTile(this.board.randomTile());
			this.armies.push(ai);
		}

		this.generator.init(this.board);
		this.generator.createObjects();

		this.active = true;
		var update = new UpdateGame();
		update.board = this.board;

		this.sendUpdate(update);
	}
}