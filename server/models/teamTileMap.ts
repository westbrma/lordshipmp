import { Tile } from "./tile";
import { Army } from "../mapObjects/army";
import { Board } from "./board";

export class TeamTileMap {
	visTiles = new Map<Tile, number>();
	team: Army;
	board: Board;

	constructor(team: Army) {
		this.team = team;
		this.board = team.board;
	}

	isTileVisible(tile) {
		return this.visTiles.has(tile);
	}

	exposeTile(tile) {
		if(tile == null){
			throw new Error('invalid tile');
		}
		this.visTiles.set(tile, 0);
	}

	exposeTiles(source: Tile, radius: number) {
		for (var x = -radius; x <= radius; x++) {
			for (var y = -radius; y <= radius; y++) {
				var tile = this.board.tile(x + source.x, y + source.y);
				if (tile != null && source.getRawDistanceFrom(tile) <= radius) {
					this.exposeTile(tile);
				}
			}
		}
	}

	getVisTiles(): IVisTiles {
		var result: IVisTiles = {
			x:[],
			y:[]	
		};

		var iter = this.visTiles.keys();
		var next = iter.next();
		while (next.value) {
			result.x.push(next.value.x);
			result.y.push(next.value.y);
			next = iter.next();
		}
		return result;
	}

	isTileExposed(center: Tile, radius, tile: Tile) {
		return center.getRawDistanceFrom(tile) <= radius;
	}

	startTurn() {
		var iter = this.visTiles.entries();
		var next = iter.next();
		while (!next.done) {
			this.processTile(next.value[0], next.value[1]);
			next = iter.next();
		}
	}

	processTile(tile: Tile, value: number) {
		if (value == 0) {
			if (this.isTileExposed(this.team.tile, ViewDistance, tile)) {
				return;
			}

			for (let i = 0; i < this.team.cities.length; i++) {
				var city = this.team.cities[i];
				for (let j = 0; j < city.tiles.length; j++) {
					if (this.isTileExposed(city.tiles[j], CityView, tile)) {
						return;
					}
				}
			}

			if(this.team.expeditions.find(e => e.target.tile == tile || e.tile == tile) != null){
				return;
			}
		}

		value += 1;
		if (value > 3) {
			this.visTiles.delete(tile);
		}
		else {
			this.visTiles.set(tile, value);
		}
	}
}