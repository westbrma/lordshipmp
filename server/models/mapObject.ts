import { Tile } from "./tile";
import { helper } from "../helper";
import { Game } from "./game";
import { Army } from "../mapObjects/army";
import { Board } from "./board";
import { Expedition } from "./expedition";
import { Objective } from "../objectives/objective";
import { Skills } from "./skill";
import { Card } from "./card";
import { SkillObjective } from "../objectives/skillObjective";
import { Trap } from "../objectives/trap";
import { Combat } from "../objectives/combat";
import { Reward } from "./reward";

export class MapObject {
	id:number;
	type:ObjType = ObjType.Animal;
	tile:Tile;
	icon:string;
	destroyed:boolean = false;

	game:Game;
	team:Army = null;
	collectable:boolean = false;
	
	hp:number = 0;
	name:string = null;

	engagements:Expedition[] = [];
	objectives: Objective[] = [];
	reward: Reward = null;
	skills: Skills = new Skills();

	changed:boolean = true;

	constructor(type:ObjType, game:Game){
		this.id = helper.genID();
		this.type = type;
		this.game = game;
		this.game.addObject(this);
	}
	
	get board():Board {
		return this.game.board;
	}

	startTurn() {
	}

	get isBattleTarget(){
		return this.hp > 0;
	}

	setSkill(type:SkillType, amount:number){
		this.skills.map[type] = amount;
		this.skills.set();
	}

	getSkill(type: SkillType) {
		var result = this.skills.map[type];
		if(result == undefined){
			result = 0;
		}

		return result;
	}

	setTile(tile: Tile) {
		if (this.tile != null){
			if(this.tile.occupant != this) {
				throw new Error('something when wrong');
			}
			this.tile.occupant = null;
		}

		this.tile = tile;
		if (this.tile != null) {
			if (tile.occupant != null) {
				throw Error("Already occupied");
			}

			this.tile.occupant = this;
		}
	}

	getDistanceFrom(mapObject: MapObject): number {
		return this.tile.getDistanceFrom(mapObject.tile);
	}

	complete(expedition:Expedition) {
		this.destroy();
	}

	engage(expedition:Expedition){
		this.engagements.push(expedition);
	}

	removeEngagement(expedition:Expedition){
		helper.removeFromArray(expedition, this.engagements);
	}

	setHP(hp:number){
		this.hp = hp;
		this.changed = true;
	}

	takeDamage(damage: number) {
		this.hp -= damage;
		if (this.hp <= 0) {
			this.destroy();
		}
		this.changed = true;
	}

	destroy() {
		if(this.tile){
			this.tile.occupant = null;
			this.tile = null;
		}

		this.destroyed = true;
		this.game.removeObject(this);
	}

	setReward(type: RewardType, amount: number, item?: Card) {
		this.reward = {
			type: type,
			amount: amount,
			item: item
		};
	}

	addObjective(obj: Objective) {
		obj.id = this.objectives.length + 1;
		this.objectives.push(obj);
	}

	addRandomTask(chance): boolean {
		var onWater = false;//this.tile.type == TileType.Water;
		if (helper.getRandomChance(chance)) {
			this.addObjective(new SkillObjective(onWater ? SkillType.Sailing : SkillType.Exploring, helper.getRandomInt(1, 5)));
			return true;
		}

		return false;
	}

	addRandomTrap(chance): boolean {
		if (helper.getRandomChance(chance)) {
			this.addObjective(new Trap(helper.getRandom(.45, .80)));
			return true;
		}

		return false;
	}

	addRandomCombat(chance: number, maxHP: number = 10): boolean {
		if (helper.getRandomChance(chance)) {
			this.addObjective(new Combat(helper.getRandomInt(1, maxHP)));
			return true;
		}

		return false;
	}

	getDifficulty(): number {
		var sum = 0;
		this.objectives.forEach(o => {
			if (o.amount < 1) {
				sum += Math.ceil((1 - o.amount) * 4);
			} else if (o.amount > 0) {
				sum += o.amount;
			}
		});
		return sum;
	}

	toJSON(mine?:boolean): IMapObject {
		let result:IMapObject = {
			id: this.id,
			type: this.type,
			tile: { x: this.tile.x, y: this.tile.y },
			hp: this.hp,
			skills: this.skills.map,
			objectives: []
		}

		if(this.team){
			result.teamId = this.team.id;
		}

		if(this.reward){
			result.reward = {
				type: this.reward.type,
				amount: this.reward.amount
			};
		}

		this.objectives.forEach(o => {
			result.objectives.push({
				type: o.type,
				skill: o.skill,
				amount: o.amount,
				itemName: o.itemName
			});
		});

		return result;
	}
}