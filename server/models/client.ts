import * as WebSocket from 'ws';
import { Player } from './player';
import { Game } from './game';

export class Client {
	id:number;
	socket: WebSocket;
	player: Player;
	game: Game;
}