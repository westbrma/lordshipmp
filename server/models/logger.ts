import * as fs from 'fs'
var util = require('util');
var log_file = fs.createWriteStream(__dirname + '/debug.log', { flags: 'w' });
var error_file = fs.createWriteStream(__dirname + '/error.log', { flags: 'a' });
var log_stdout = process.stdout;
var error_stdout = process.stderr;

export var onLog = function (d:string) {
	log_file.write(util.format(d) + '\n');
	log_stdout.write(util.format(d) + '\n');
};

export var onError = function(d:string){
	error_file.write(util.format(d) + '\n');
	error_stdout.write(util.format(d) + '\n');
}