import { Tile } from "./tile";
import { helper } from "../helper";
import { Game } from "./game";

export class Board {
	width: number;
	height: number;
	tiles: Tile[][] = [];
	tileCount: number;
	game:Game;

	constructor(width, height, game:Game) {
		this.game = game;
		this.width = width;
		this.height = height;
		this.tileCount = width * height;
		this.tiles = [];
		this.generate();
	}

	tile(x: number, y: number) {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
			return null;
		}
		return this.tiles[y][x];
	}

	calcTileCount(count, perTiles): number {
		return Math.ceil(count * (this.tileCount / perTiles));
	}

	randomTile(noVillage?:boolean): Tile {
		var tile: Tile = null;
		while (tile == null) {
			var x = helper.getRandomInt(0, this.width -  1);
			var y = helper.getRandomInt(0, this.height - 1);
			tile = this.tile(x, y);
			if (tile.occupant != null || noVillage && tile.type == TileType.Village) {
				tile = null;
			}
		}

		return tile;
	}

	generate() {
		let arr:TileType[] = [TileType.Mountain, TileType.Grass, TileType.Trees ];

		var w = this.width;
		var h = this.height;
		for (var y = 0; y < h; y++) {
			var row: Tile[] = [];
			this.tiles.push(row);

			for (var x = 0; x < w; x++) {
				var type: TileType = helper.getRandomItem(arr);
				var tile = new Tile(x, y, type, this);
				row.push(tile);
			}
		}

		var waterTiles = this.calcTileCount(helper.getRandomInt(4, 20), 100);
		var sandTiles = this.calcTileCount(helper.getRandomInt(4, 20), 100);
		var dirtTiles = this.calcTileCount(helper.getRandomInt(4, 20), 100);

		var sheepTiles = this.calcTileCount(50, 1800);
		var villageTiles = this.calcTileCount(30, 1800);
		var goldTiles = this.calcTileCount(30, 1800);
		var farmTiles = this.calcTileCount(30, 1800);

		// sand
		var sandCount = 0;
		while (sandCount < sandTiles) {
			sandCount += this.createTileGroup(TileType.Sand, 6, 20);
		}

		// dirt
		var dirtCount = 0;
		while (dirtCount < dirtTiles) {
			dirtCount += this.createTileGroup(TileType.Dirt, 6, 20);
		}

		// water
		var wCount = 0;
		while (wCount < waterTiles) {
			wCount += this.createTileGroup(TileType.Water, 6, 20);
		}

		// add sheep
		for (var i = 0; i < sheepTiles; i++) {
			this.randomTile().type = TileType.Sheep;
		}

		// add villages
		for (var i = 0; i < villageTiles; i++) {
			this.randomTile().type = TileType.Village;
		}

		// add gold
		for (var i = 0; i < goldTiles; i++) {
			this.randomTile().type = TileType.Gold;
		}

		// add farm
		for (var i = 0; i < farmTiles; i++) {
			this.randomTile().type = TileType.Farm;
		}
	}

	getRandomAdjTile(tile: Tile): Tile {
		let move = helper.getRandomInt(0, 3);
		let result: Tile = null;
		switch (move) {
			case 0:
				result = this.tile(tile.x, tile.y - 1);
				break;
			case 1:
				result = this.tile(tile.x - 1, tile.y);
				break;
			case 2:
				result = this.tile(tile.x, tile.y + 1);
				break;
			case 3:
				result = this.tile(tile.x + 1, tile.y);
				break;
		}
		return result;
	}

	createTileGroups(type: TileType, minCount: number, maxCount: number, minSize: number, maxSize: number) {
		var count = helper.getRandomInt(minCount, maxCount);

		for (let i = 0; i < count; i++) {
			this.createTileGroup(type, minSize, maxSize);
		}
	}

	createTileGroup(type: TileType, minSize: number, maxSize: number): number {
		var result = 0;
		var prev: Tile = this.randomTile();
		prev.type = type;

		var size = helper.getRandomInt(minSize, maxSize);
		let j = 1;

		while (j < size) {
			let next: Tile = this.getRandomAdjTile(prev);

			if (next != null) {
				if (next.type != type) {
					next.type = type;
					result++;
				}
				j++;
				prev = next;
			}
		}

		return result;
	}

	toJSON():IBoard{
		var result:IBoard = {
			width: this.width,
			height: this.height,
			tiles: []
		}

		for(let y=0; y<this.height; y++){
			var row:TileType[] = [];
			result.tiles.push(row);
			for(let x=0; x<this.width; x++){
				row.push(this.tiles[y][x].type);
			}
		}

		return result;
	}
}