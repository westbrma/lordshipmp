var enums = require('../shared/enums');
//var globals = require('../shared/globals');
var skillDefs = require('../shared/defs/skillDefs');
var cardDefs = require('../shared/defs/cardDefs');
var unitDefs = require('../shared/defs/unitDefs');
var tileDefs = require('../shared/defs/tileDefs');
var tileDefs = require('../shared/defs/prodDefs');
var pos2 = require('../shared/position2');

import * as express from 'express';
import { GameSocket } from './gameSocket';
import * as http from 'http';
import * as WebSocket from 'ws';
import {onLog, onError} from './models/logger'

console.log = onLog;
console.error = onError;

var port = process.env.PORT || 3000;

var app = express();
app.use('/', express.static('build'));

var server = app.listen(port, function () {
	console.log(`Example app listening on ${port} !`);
});

var wss = new WebSocket.Server({ server });
var socket = new GameSocket(wss);