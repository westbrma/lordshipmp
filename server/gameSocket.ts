import * as WebSocket from 'ws';
import { Client } from './models/client';
import { helper } from './helper';
import { Game } from './models/game';
import { Player } from './models/player';

export let socket: GameSocket = null;

export class GameSocket {
	clients: Client[] = [];
	clientMap: { [key: number]: Client } = {};
	games: Game[] = [];

	constructor(wss: WebSocket.Server) {
		socket = this;
		wss.on('connection', (ws: WebSocket) => {
			this.onConnection(ws);
		});
	}

	onConnection(ws: WebSocket) {
		let client = new Client();
		client.id = helper.genID();
		client.socket = ws;
		this.clients.push(client);
		this.clientMap[client.id] = client;
		this.sendGameList(client);

		ws.on('message', (data: string) => {
			//try {
				console.log('received: ' + data);
				var msg: IMessage = JSON.parse(data);
				switch (msg.type) {
					case MessageType.JoinGame:
						this.onJoinGame(client, msg as IJoinGameMsg);
						break;
					case MessageType.LeaveGame:
						this.onLeaveGame(client, msg as ILeaveGame);
						break;
					case MessageType.PostMessage:
						this.broadcast(msg, client.game);
						break;
					case MessageType.TakeTurn:
						client.game.takeTurn(client.player, msg as ITakeTurn);
						break;
					case MessageType.StartGame:
						this.onStartGame(client, msg as IStartGame);
						break;
					case MessageType.CreateGame:
						this.onCreateGame(client, msg as ICreateGameMsg);
						break;
					case MessageType.UpdatePlayer:
						this.onUpdatePlayer(client, msg as IUpdatePlayer)
						break;
				}
			// } catch (err) {
			// 	if (client.game) {
			// 		this.broadcastError(err.message, client.game);
			// 		this.destroyGame(client.game);
			// 	}
			// }
		});
		ws.on('close', () => {
			this.removeClient(client);
		});
	}

	broadcastError(message: string, game: Game) {
		var msg: IMessage = {
			type: MessageType.Error,
			error: message
		}

		this.broadcast(msg, game);
	}

	broadcast(message: IMessage, game: Game) {
		game.players.forEach(p => {
			this.sendMessage(p.client, message);
		})
	}

	broadcastPost(message: string, game: Game) {
		var post: IPostMsg = {
			type: MessageType.PostMessage,
			messsage: message
		}

		game.players.forEach(p => {
			this.sendMessage(this.clientMap[p.id], post);
		})
	}

	sendMessage(client: Client, message: IMessage) {
		try {
			var data = JSON.stringify(message);
			console.log('Sending: ' + data);
			client.socket.send(data);
		} catch (error) {
			this.removeClient(client);
			console.log(error);
		}
	}

	onUpdatePlayer(client:Client, msg:IUpdatePlayer){
		client.player.color = msg.player.color;
		client.game.boardcastInfo();
	}

	onCreateGame(client: Client, msg: ICreateGameMsg) {
		if (client.game == null) {
			var game = new Game();
			game.id = helper.genID();
			game.ownerId = client.id;
			game.maxPlayers = 6;
			game.topScore = 100;
			this.games.push(game);
			msg.game = game;
		} else {
			msg.error = 'You cannot create a game because you are already in a game.';
		}

		this.sendMessage(client, msg);
		this.sendGameList();
	}

	onStartGame(client: Client, msg: IStartGame) {
		if (client.id == client.game.ownerId) {
			client.game.start(msg);
		} else {
			msg.error = 'Only the owner can start the game.';
			this.sendMessage(client, msg);
		}
	}

	sendGameList(client?: Client) {
		var msg: IGameListMsg = {
			type: MessageType.GameList,
			games: []
		}

		this.games.forEach(g => {
			msg.games.push(g.toJSON(false));
		});

		if (client) {
			this.sendMessage(client, msg);
		} else {
			this.clients.forEach(c => {
				if (c.game == null) {
					this.sendMessage(c, msg);
				}
			});
		}
	}

	removeClient(client: Client) {
		var idx = this.clients.indexOf(client);
		if (idx >= 0) {
			this.clients.splice(idx, 1);
			this.clientMap[client.id] = undefined;
			if (client.game) {
				client.game.removePlayer(client.player);
			}
			console.log('remove client');
		}
	}

	onJoinGame(client: Client, msg: IJoinGameMsg) {
		var game = this.games.find(g => g.id == msg.gameId);
		if (game) {
			game.addPlayer(client, msg)
		}
	}

	onLeaveGame(client: Client, msg: ILeaveGame) {
		if (client.game && client.game.id == msg.gameId) {
			var game = client.game;
			client.game = null;
			game.removePlayer(client.player);
		}
	}

	destroyGame(game: Game) {
		helper.removeFromArray(game, this.games);
		this.broadcast({ type: MessageType.GameDestroyed }, game);
		game.players.forEach(p => {
			this.clientMap[p.id].game = null;
		});
		this.sendGameList();
	}
}