import { helper } from "../helper";
import { Unit, Card } from "../models/card";

export let unitGenerator = {
	createUnit: function (): Unit {
		var type = helper.getRandomItem(unitList).name;
		var unit = new Unit(<UnitType>type);
		return unit;
	}
}

export let cardGenerator = {
	getChestItem: function (): Card {
		var def = helper.getRandomItem(chestList);
		var card = new Card(def);
		return card;
	}
}