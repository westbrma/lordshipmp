import { helper } from "../helper";

class UnitNames {
	free: string[];
	list: string[] = [
		'Bart',
		'Mark',
		'Sarah',
		'John',
		'Bob',
		'Holden',
		'Jerry',
		'Lamar',
		'Tom',
		'Pete',
		'Sassy',
		'Noah',
		'Liam',
		'Mason',
		'Jacob',
		'William',
		'Ethan',
		'James',
		'Alexander']

	constructor() {
		this.reset();
	}

	reset() {
		this.free = this.list.slice();
		helper.shuffleArray(this.free);
	}

	getUnitName() {
		return helper.getRandomItem(this.list);
	}

	getLordName() {
		return this.free.pop();
	}
}

export let unitNames = new UnitNames();