import { Game } from "../models/game";
import { Board } from "../models/board";
import { MapObject } from "../models/mapObject";
import { helper } from "../helper";
import { Chest } from "../mapObjects/chest";
import { Animal } from "../mapObjects/animal";
import { Discovery } from "../mapObjects/discovery";
import { Creep } from "../mapObjects/creep";
import { Recruit } from "../mapObjects/recruit";
import { Merchant } from "../mapObjects/merchant";
import { JobPosting } from "../mapObjects/jobPosting";

export class ObjGenerator {
	recruits: number = 0;
	food: number = 0;
	discoveries: number = 0;
	creeps: number = 0;
	merchants: number = 0;
	chests: number = 0;
	postings: number = 0;

	totalCreeps: number = 0;
	totalDiscoveries: number = 0;
	totalFood: number = 0;
	totalRecruits: number = 0;
	totalMerchants: number = 0;
	totalChests: number = 0;
	totalPostings: number = 0;

	game: Game;
	board: Board;

	constructor(game: Game) {
		this.game = game;
	}


	init(board: Board) {
		this.board = board;

		this.recruits = 0;
		this.food = 0;
		this.discoveries = 0;
		this.creeps = 0;
		this.merchants = 0;
		this.chests = 0;

		this.totalCreeps = board.calcTileCount(12, 1000);
		this.totalDiscoveries = board.calcTileCount(13, 1000);
		this.totalFood = board.calcTileCount(15, 1000);
		this.totalRecruits = board.calcTileCount(15, 1000);
		this.totalMerchants = board.calcTileCount(15, 1000);
		this.totalChests = board.calcTileCount(12, 1000);
		this.totalPostings = board.calcTileCount(2, 1000);
	}

	removeObject(o: MapObject) {
		this.addObject(o, -1);
	}

	addObject(o: MapObject, count: number) {
		switch (o.type) {
			case ObjType.Animal:
				this.food += count;
				break;
			case ObjType.Discovery:
				this.discoveries += count;
				break;
			case ObjType.Creep:
				this.creeps += count;
				break;
			case ObjType.Recruit:
				this.recruits += count;
				break;
			case ObjType.Merchant:
				this.merchants += count;
				break;
			case ObjType.Chest:
				this.chests += count;
				break;
			case ObjType.Chest:
				this.chests += count;
				break;
			case ObjType.Posting:
				this.postings += count;
				break;
		}
	}

	createObjects() {
		this.genCreeps(this.totalCreeps - this.creeps);
		this.genDiscoveries(this.totalDiscoveries - this.discoveries);
		this.genFood(this.totalFood - this.food);
		this.genRecruits(this.totalRecruits - this.recruits);
		this.genMerchants(this.totalMerchants - this.merchants);
		this.genChests(this.totalChests - this.chests);
		this.genPosting(this.totalPostings - this.postings);
	}

	genPosting(count: number) {
		for (var i = 0; i < count; i++) {
			new JobPosting(this.board.randomTile(true), this.game);
		}
	}

	genChests(count: number) {
		for (var i = 0; i < count; i++) {
			new Chest(this.board.randomTile(true), this.game);
		}
	}

	genFood(count: number) {
		for (var i = 0; i < count; i++) {
			new Animal(this.board.randomTile(true), this.game);
		}
	}

	genMerchants(count: number) {
		for (var i = 0; i < count; i++) {
			new Merchant(this.board.randomTile(true), this.game);
		}
	}

	genRecruits(count: number) {
		for (var i = 0; i < count; i++) {
			new Recruit(this.board.randomTile(true), this.game);
		}
	}

	genCreeps(count: number) {
		for (var i = 0; i < count; i++) {
			new Creep(this.board.randomTile(true), this.game);
		}
	}

	genDiscoveries(count: number) {
		for (var i = 0; i < count; i++) {
			new Discovery(this.board.randomTile(true), this.game);
		}
	}
}