import { Army } from "./army";
import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { Game } from "../models/game";
import { Expedition } from "../models/expedition";
import { City } from "../models/city";
import { Objective } from "../objectives/objective";
import { helper } from "../helper";

interface IObjInfo {
	obj: MapObject;
	rank: number;
	distance: number;
	food: number;
}

const TwoPi = Math.PI * 2;

export class AIArmy extends Army {
	objects: IObjInfo[] = [];
	villages: Map<Tile, boolean> = new Map();
	_exploreTile: Tile;
	_targetObj: IObjInfo;
	_newCityTile: Tile;

	constructor(game: Game) {
		super(game, 'bob', helper.genID());
	}

	inform(message: string, error?:boolean){
		if(error){
			throw new Error(message);
		}
	}

	startTurn() {
		super.startTurn();
		this._targetObj = null;
		this.manageCities();
		this.setObjects();
		this.createExpeditions();
		this.setMove();
	}

	setMove() {
		if (this.cards.length == 0 || this.food <= 5) {
			var village = this.getVillageTile();
			if (village != null) {
				this.doAIMove(village);
				return;
			}
		}

		if (this.canAffordCity && this.game.round > 5) {
			if (this._newCityTile == null || !City.canBuild(this._newCityTile)) {
				this._newCityTile = this.getNewCityTile();
			}

			if (this.tile == this._newCityTile) {
				var city = this.buildCity();
			} else {
				this.doAIMove(this._newCityTile);
			}

			return;
		}

		if (this._targetObj != null) {
			var moveTile = this.getClosestVisTile(this._targetObj.obj.tile);
			this.doAIMove(moveTile);
		} else {
			while (!this._exploreTile || this._exploreTile.occupant || this.tile == this._exploreTile) {
				this.setExploreDest();
			}

			this.doAIMove(this._exploreTile);
		}
	}

	doAIMove(tile: Tile) {
		if (!this.isTileVisible(tile) || tile.occupant != null) {
			tile = this.getClosestVisTile(tile);
		}

		this.moveTo(tile);
	}

	getVillageTile(): Tile {
		var result: Tile = null;
		var closestDist: number = 0;

		for (const t of this.villages.keys()) {
			if (t.occupant != null) continue;

			var dist = this.tile.getDistanceFrom(t);
			if (dist < closestDist || result == null) {
				result = t;
				closestDist = dist;
			}
		}

		return result;
	}

	getClosestVisTile(dest: Tile) {
		var result: Tile = null;
		var closestDist: number = 0;
		
		var iter = this.tileMap.visTiles.keys();
		var next = iter.next();
		while (!next.done) {
			let t = next.value;
			next = iter.next();

			if (t.occupant == null) {
				var dist = t.getDistanceFrom(dest);
				if (dist < closestDist || result == null) {
					result = t;
					closestDist = dist;
				}
			}
		}

		return result;
	}

	manageCities() {
		this.cities.forEach(c => {
			if (c.expPts < 1) return;

			var options = this.getExpandOptions(c, c.expPts);

			for (let i = 0; i < options.length; i++) {
				var n = options[i];
				let cost = n.city == null ? 1 : 2;
				if(c.expPts >= cost){
					c.expand(n);
				}else{
					break;
				}
			}
		});
	}

	// build list of all expand options.
	getExpandOptions(city: City, max: number): Tile[] {
		var options = new Map<Tile, number>();
		city.tiles.forEach(t => {
			t.adjacent.forEach(a => {
				let hasCastle = a.occupant && a.occupant.type == ObjType.City;
				if (!hasCastle && !options.has(a) && a.team != this.team) {
					var def = a.def;
					var rank = def.Population + def.Gold + def.Food;
					if(a.city){
						rank -= 2;
					}
					options.set(a, rank);
				}
			});
		});

		var result = Array.from(options.keys());
		result.sort((a, b) => {
			return options.get(b) - options.get(a);
		});

		return result;
	}

	createExpeditions() {
		for (let i = 0; i < this.objects.length; i++) {
			if (this.nonActive == 0) return;

			let info = this.objects[i];
			let obj = info.obj;

			if (info.distance > 2) {
				this._targetObj = info;
				return;
			}

			if (obj.hp > 0 && obj.hp > this.nonActiveHP) {
				return;
			}

			var exp = new Expedition(this, obj);
			this.cards.forEach(u => {
				if (u.expedition == null) {
					exp.addCard(u);
				}
			});

			exp.start(obj);
			this.calcHP();
		}
	}

	setObjects() {
		var needsFood = this.food < 10;
		this.objects = [];

		var tiles = this.team.tileMap.visTiles;
		for (var [tile, value] of tiles) {
			if (tile.type == TileType.Village) {
				if (!this.villages.has(tile)) {
					this.villages.set(tile, true);
				}
			}

			if (tile.occupant && tile.occupant.team != this.team) {
				if(tile.occupant.type == ObjType.Army){
					continue;
				}

				if (!this.hasRequiredItems(tile.occupant)) {
					continue;
				}

				var info: IObjInfo = {
					obj: tile.occupant,
					rank: 0,
					distance: this.tile.getDistanceFrom(tile),
					food: 0
				}

				info.rank = -info.distance;

				if (needsFood && tile.occupant.reward && tile.occupant.reward.type == RewardType.Food) {
					var food = tile.occupant.reward.amount
					info.rank += food;
				} else {
					//info.rank += mission.reward.amount;
				}

				this.objects.push(info);
			}
		}

		if (this.objects.length > 1) {
			this.objects.sort((a, b) => b.rank - a.rank);
		}
	}

	// gold, food, points, recruits, chests
	shouldTakeMission(target: MapObject): boolean {
		if (!this.hasRequiredItems(target)) {
			return false;
		}

		target.objectives.forEach(o => {
			if (o.isChance) {
				var chance = this.calcChance(o.skill, o);
				if (chance < .6) {
					return false;
				}
			}
		});
	}

	calcChance(skill: SkillType, objective: Objective): number {
		var stat = 0;
		this.cards.forEach(u => {
			stat += u.getSkill(skill);
		});
		stat *= .1;

		var chance = objective.amount + stat;
		if (chance > 1) {
			chance = 1;
		}
		return chance;
	}

	setExploreDest() {
		var best = null;
		var bestScore = -1;
		var angle = helper.getRandom(0, TwoPi);
		var step = TwoPi / 5;

		for (let i = 0; i < 5; i++) {
			let v: Position2 = helper.angleToVector(angle);
			v.x *= 6;
			v.y *= 6;

			let x = Math.floor(v.x);
			let y = Math.floor(v.y);

			let tile = this.board.tile(x + this.tile.x, y + this.tile.y);
			if (tile != null && tile.occupant == null) {
				let score = this.getTileExploreScore(tile, 1);
				if (score > bestScore) {
					best = tile;
					bestScore = score;
				}
			}

			angle += step;
		}

		this._exploreTile = best;
	}

	getTileExploreScore(source: Tile, radius: number) {
		var result = 0;

		for (let x = -radius; x <= radius; x++) {
			for (let y = -radius; y <= radius; y++) {
				var tile = this.board.tile(x + source.x, y + source.y);
				if (tile != null) {
					if (!this.isTileVisible(tile)) {
						result++;
					}
				}
			}
		}

		return result;
	}

	getNewCityTile(): Tile {
		var tile: Tile = null;
		var best: number = 0;

		// iterate vis tiles, calc each vis tiles new city rank
		this.tileMap.visTiles.forEach((v, t) => {
			if (City.canBuild(t)) {
				let rank = this.calcCityTileRank(t);
				if (rank > best) {
					tile = t;
					best = rank;
				}
			}
		});

		return tile;
	}

	calcCityTileRank(tile: Tile): number {
		var rank = 0;
		for (var x = tile.x - 1; x <= tile.x + 1; x++) {
			for (var y = tile.y - 1; y <= tile.y + 1; y++) {
				var t = this.board.tile(x, y);
				if (t) {
					let def = t.def;
					rank += def.Food + def.Gold + def.Population;
				}
			}
		}

		return rank;
	}
}