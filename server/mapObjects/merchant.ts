import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { helper } from "../helper";
import { SkillObjective } from "../objectives/skillObjective";
import { Game } from "../models/game";

export class Merchant extends MapObject {
	constructor(tile: Tile, game:Game) {
		super(ObjType.Merchant, game);
		this.setTile(tile);
		this.icon = 'images/merchant.png';
		var requiredRelic = false;
		let obj = new SkillObjective(SkillType.Trading, helper.getRandomInt(2, 5));

		this.addRandomTask(.5);
		
		this.addObjective(obj);
		var gold = this.getDifficulty() * 4;
		if(requiredRelic){
			gold *= 3;
		}
		this.setReward(RewardType.Gold, gold);
	}

	startTurn() {
		super.startTurn();
	}
}