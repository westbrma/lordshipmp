import { MapObject } from "../models/mapObject";
import { City } from "../models/city";
import { Expedition } from "../models/expedition";
import { unitGenerator, cardGenerator } from "../services/unitGenerator";
import { Card, Unit } from "../models/card";
import { Tile } from "../models/tile";
import { helper } from "../helper";
import { Reward } from "../models/reward";
import { TeamTileMap } from "../models/teamTileMap";
import { Game } from "../models/game";

const cityCost: number = 100;
const buildDays: number = 3;
const MaxMove: number = 3;
const ViewDistance: number = 3;
const ChanceToStarve: number = .2;

export abstract class Army extends MapObject {
	playerType: TeamType;
	nonActive: number = 0;
	nonActiveHP: number = 0;

	tileMap: TeamTileMap;
	cards: Card[] = [];
	units: number = 0;
	cities: City[] = [];
	expeditions: Expedition[] = [];
	score: number = 0;
	gold: number = 100;
	food: number = 15;

	constructor(game: Game, name: string, id: number) {
		super(ObjType.Army, game);

		this.team = this;
		this.name = name;
		this.game = game;
		this.id = id;
		this.tileMap = new TeamTileMap(this);
		this.addCard(unitGenerator.createUnit());
	}

	gainFood(amount: number) {
		var hadFood = this.food > 0;
		this.food += amount;

		if (this.food <= 0) {
			if (hadFood) {
				this.inform('My lord, we are out of food.');
			}
			this.food = 0;
		}
	}

	moveTo(tile: Tile) {
		this.gainFood(-1);
		this.setTile(tile);
	}

	startTurn() {
		super.startTurn();
		this.tileMap.startTurn();
		this.feedArmy();
	}

	canMove(tile: Tile) {
		return this.tile.getDistanceFrom(tile) <= 1;
	}

	feedArmy() {
		if (this.food <= 0) {
			if (this.units > 0 && helper.getRandomChance(ChanceToStarve)) {
				let units = this.cards.filter(c => c instanceof Unit);
				var unit = helper.getRandomItem(units);
				var exp = unit.expedition;
				unit.destroy();
				this.team.inform(`You lost a ${unit.name} due to starvation.`);
				if (exp && exp.hp <= 0) {
					exp.destroy();
				}
			}
		}
	}

	hasRequiredItems(target: MapObject): boolean {
		var req = target.objectives.find(o => o.type == TaskType.Requirement);
		if (req) {
			var item = this.cards.find(c => c.name == req.itemName);
			return item != null;
		}
		return true;
	}

	getCollectable(tile: Tile) {
		if (tile.occupant != null && tile.occupant.collectable) {
			tile.occupant.destroy();
		}
	}

	addCard(unit: Card) {
		unit.army = this;
		this.cards.push(unit);
		this.cards.sort((a, b) => {
			if (a.def.isUnit != b.def.isUnit) {
				return a.def.isUnit ? -1 : 1;
			}

			var x = a.def.name;
			var y = b.def.name;
			return x < y ? -1 : x > y ? 1 : 0;
		});
		this.calcHP();
	}

	removeCard(unit: Card) {
		helper.removeFromArray(unit, this.cards);
		this.calcHP();
	}

	calcHP() {
		let hp = 0;
		let nonActive = 0;
		let units = 0;

		this.cards.forEach(u => {
			if (u.def.isUnit) {
				units++;

				if (u.expedition == null) {
					nonActive++;
					hp += u.hp;
				}
			}
		});

		this.units = units;
		this.nonActive = nonActive;
	}


	activateVillage(tile) {
		if (this.food == 0) {
			this.gainFood(10);
			this.inform('Food Gained: 10');
		}

		if (this.units == 0) {
			var unit = unitGenerator.createUnit();
			this.addCard(unit);
			this.inform('A ' + unit.name + ' joined the party.');
		}
	}

	setTile(tile: Tile) {
		this.getCollectable(tile);
		if (tile.type == TileType.Village) {
			this.activateVillage(tile);
		}

		super.setTile(tile);
		this.tileMap.exposeTiles(tile, ViewDistance);
		this.changed = true;
	}

	async wait(milliseconds: number) {
		return new Promise((resolve) => {
			setTimeout(() => resolve(), milliseconds);
		});
	}

	get canAffordCity(): boolean {
		return this.gold >= CityCost;
	}

	buildCity(name?: string): City {
		if(!City.canBuild(this.tile)){
			this.inform('Cannot build city here');
			return;
		}

		this.spendGold(CityCost);
		return new City(this, this.game, name);
	}

	spendGold(amount: number) {
		this.gold -= amount;
		if (this.gold < 0) {
			throw new Error('Insufficeint Gold');
		}
	}

	abstract inform(message: string, error?:boolean);

	isTileVisible(tile) {
		return this.tileMap.isTileVisible(tile);
	}

	recieveRewards(rewards: Reward[]) {
		rewards.forEach(r => {
			switch (r.type) {
				case RewardType.Food:
					this.gainFood(r.amount);
					this.inform('Food Gained: ' + r.amount);
					break;
				case RewardType.Gold:
					this.gold += r.amount;
					this.inform('Gold Gained: ' + r.amount);
					break;
				case RewardType.Score:
					this.score += r.amount;
					this.inform('+' + r.amount + ' Fame');
					break;
				case RewardType.Unit:
				case RewardType.Item:
					this.addCard(r.item);
					this.inform('A ' + r.item.name + ' joined the party.');
					break;
				case RewardType.Chest:
					let card = cardGenerator.getChestItem();
					this.addCard(card);
					this.inform('You received a ' + card.name + '.');
					break;
			}
		});
	}

	destroy(){
		super.destroy();
		console.log('blkah');
	}

	toJSON(): IArmy {
		var result:IArmy = <IArmy>super.toJSON();
		result.score = this.score;
		return result;
	}
}