import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { helper } from "../helper";
import { Card } from "../models/card";
import { Combat } from "../objectives/combat";
import { Game } from "../models/game";

export class Creep extends MapObject
{
	originalHP:number = 0;

	constructor(tile:Tile, game:Game){
		super(ObjType.Creep, game);
		this.setTile(tile);

		this.hp = this.originalHP = helper.getRandomInt(2, 10);
		let combat = helper.getRandomInt(0, 3);
		this.setSkill(SkillType.Combat, combat);

		if (helper.getRandomChance(.7)) {
			let scoreMod = Math.ceil(this.hp * (combat * .25));
			this.setReward(RewardType.Score, this.hp + scoreMod);
		} else {
			this.setReward(RewardType.Item, 0, new Card(cardMap[ItemType.Princess]));
		}
	}

	startTurn(){
		if(this.hp != this.originalHP && this.engagements.length == 0){
			this.setHP(this.originalHP)
		}
	}
}