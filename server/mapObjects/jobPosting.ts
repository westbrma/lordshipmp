import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { helper } from "../helper";
import { SkillObjective } from "../objectives/skillObjective";
import { Requirement } from "../objectives/requirement";
import { Game } from "../models/game";

export class JobPosting extends MapObject {
	constructor(tile: Tile, game: Game) {
		super(ObjType.Posting, game);
		this.setTile(tile);

		this.icon = 'images/mission.png';
		this.setReward(RewardType.Score, 10);
		this.addObjective(new Requirement('Princess'));
	}

	startTurn() {
		super.startTurn();
	}
}