import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { helper } from "../helper";
import { ChanceObjective } from "../objectives/chanceObjective";
import { Game } from "../models/game";

export class Chest extends MapObject {
	constructor(tile: Tile, game:Game) {
		super(ObjType.Chest, game);
		this.setTile(tile);
		this.icon = (tile.type == TileType.Water) ? 'images/waterMission.png' : 'images/chest.png';
		let obj = new ChanceObjective(SkillType.Locks, helper.getRandom(.45,.8));
		this.setReward(RewardType.Chest, 0);
		this.addRandomTask(.5);
		if(!this.addRandomCombat(.25, 5)){
			this.addRandomTrap(.3);
		}
		this.addObjective(obj);
	}

	startTurn() {
		super.startTurn();
	}
}