import { Army } from "./army";
import { Player } from "../models/player";
import { Game } from "../models/game";
import { Expedition } from "../models/expedition";

export class PlayerArmy extends Army {
	turn: ITakeTurn;
	informs: string[] = [];

	constructor(game: Game, player: Player) {
		super(game, player.name, player.id);
	}

	inform(message: string, error?: boolean) {
		this.informs.push(message);
	}

	startTurn() {
		super.startTurn();

		let turn = this.turn;
		let game = this.game;

		turn.newExpeditions.forEach(e => {
			var mapObject = game.mapObjectMap.get(e.objectId);
			if (mapObject != null) {
				var exp = new Expedition(this, mapObject);
				e.cardIds.forEach(id => {
					var card = this.cards.find(c => c.id == id);
					if (card != null) {
						exp.addCard(card);
					}
				});
				
				exp.start(mapObject);
			}else{
				console.log('invalid');
			}
		});

		turn.cityUpdates.forEach(c => {
			if (c.newCity) {
				let city = this.buildCity(c.cityName);
				this.cities.push(city);
			} else {
				let city = this.cities.find(city => city.id == c.cityId);
				c.expansions.forEach(e => {
					var tile = this.board.tile(e.x, e.y);
					city.expand(tile);
				});

				if (c.prodType) {
					city.activeProd = prodMap[c.prodType];
				}
			}
		})

		if (turn.move) {
			var tile = this.board.tile(turn.move.x, turn.move.y);

			if (!this.isTileVisible(tile)) {
				throw new Error('can only move to visible tiles');
			}

			var dist = this.tile.getDistanceFrom(tile);
			if (dist > 1) {
				throw new Error('You can only move within 3 tiles');
			}

			if (tile.occupant != null) {
				tile = tile.getRandomNeighbor(true, false);
			}

			if (tile.occupant == null) {
				this.moveTo(tile);
			}
		}

		this.turn = null;
	}

	getMyInfo(): IMyInfo {
		var result: IMyInfo = {
			informs: this.informs,
			gold: this.gold,
			food: this.food,
			cards: [],
			visTiles: this.tileMap.getVisTiles()
		}

		this.cards.forEach(c => {
			result.cards.push(c.toJSON());
		});

		return result;
	}
}