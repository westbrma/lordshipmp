import { Tile } from "../models/tile";
import { MapObject } from "../models/mapObject";
import { unitGenerator } from "../services/unitGenerator";
import { helper } from "../helper";
import { ChanceObjective } from "../objectives/chanceObjective";
import { SkillObjective } from "../objectives/skillObjective";
import { Game } from "../models/game";

export class Recruit extends MapObject {
	constructor(tile: Tile, game:Game) {
		super(ObjType.Recruit, game);
		this.setTile(tile);
		this.icon = 'images/recruit.png';

		var unit = unitGenerator.createUnit();
		this.setReward(RewardType.Unit, 0, unit);
		this.addRandomTask(.5);
		//this.mission.addRandomCombat(.2);

		if (helper.getRandomChance(.25)) {
			let obj = new ChanceObjective(SkillType.Charisma, .5);
			this.addObjective(obj);
		} else {
			let obj = new SkillObjective(SkillType.Charisma, 3);
			this.addObjective(obj);
		}
	}

	startTurn() {
		super.startTurn();
	}
}