import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { helper } from "../helper";
import { ChanceObjective } from "../objectives/chanceObjective";
import { SkillObjective } from "../objectives/skillObjective";
import { Game } from "../models/game";

export class Animal extends MapObject {
	constructor(tile:Tile, game:Game) {
		super(ObjType.Animal, game);
		this.setTile(tile);

		var fishing = tile.type == TileType.Water;

		if(fishing){
			this.icon = 'images/fish.png';
		}else{
			this.icon = 'images/animal.png';
		}

		var obj = new ChanceObjective(fishing ? SkillType.Fishing : SkillType.Hunting, helper.getRandom(.30,.90));
		this.addObjective(new SkillObjective(fishing ? SkillType.Sailing : SkillType.Exploring, helper.getRandomInt(1,5)));
		this.addObjective(obj);
		this.setReward(RewardType.Food, this.getDifficulty() * 3);
	}

	startTurn() {
		super.startTurn();
	}
}