import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { helper } from "../helper";
import { SkillObjective } from "../objectives/skillObjective";
import { Requirement } from "../objectives/requirement";
import { Game } from "../models/game";

export class Discovery extends MapObject {
	constructor(tile: Tile, game: Game) {
		super(ObjType.Discovery, game);
		this.setTile(tile);

		this.icon = tile.type == TileType.Water ? 'images/waterMission.png' : 'images/wonder.png';
		var amount = helper.getRandomInt(1, 5);
		let obj = new SkillObjective(SkillType.Exploring, amount);
		if (!this.addRandomCombat(.5)) {
			this.addRandomTrap(.99);
		}
		this.addObjective(obj);
		this.setReward(RewardType.Score, Math.round(this.getDifficulty() * .5));
	}

	startTurn() {
		super.startTurn();
	}
}