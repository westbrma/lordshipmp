import { Expedition, ObjStatus } from "../models/expedition";
import { helper } from "../helper";
import { Objective } from "./objective";

export class Combat extends Objective {
	isCombat:boolean = true;

	constructor(hp:number) {
		super(TaskType.Combat);
		this.skill = SkillType.Combat;
		this.amount = hp;
	}

	startTurn(exp: Expedition, status:ObjStatus) {
		if(helper.getRandomChance(.5)){
			exp.takeDamage(1);
		}else{
			status.countDown--;
		}

		status.progress = status.countDown;
		if (status.countDown <= 0) {
			status.status = ExpeditionStatus.Completed;
		}
		
		if(exp.hp <= 0){
			status.status = ExpeditionStatus.Failed;
		}
	}
}