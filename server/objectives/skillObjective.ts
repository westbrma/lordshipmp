import { Objective } from "./objective";
import { Expedition, ObjStatus } from "../models/expedition";

export class SkillObjective extends Objective {

	constructor(skill: SkillType, amount: number){
		super(TaskType.Skill);
		this.skill = skill;
		this.amount = amount;
	}

	startTurn(exp: Expedition, status:ObjStatus) {
		if(status.progress == 0){
			status.progress += exp.getSkill(this.skill);
		}
		
		status.progress++;
		status.countDown = this.amount - status.progress;
		if (status.progress >= this.amount) {
			status.progress = this.amount;
			status.status = ExpeditionStatus.Completed;
		}
	}
}