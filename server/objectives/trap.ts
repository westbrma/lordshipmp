import { ChanceObjective } from "./chanceObjective";
import { ObjStatus, Expedition } from "../models/expedition";

export class Trap extends ChanceObjective {
	constructor(chance: number) {
		super(SkillType.Traps, chance);
	}

	startTurn(exp: Expedition, status:ObjStatus) {
		super.startTurn(exp, status);
		if(status.failed){
			exp.takeDamage(5);
			if(exp.hp > 0){
				status.status = ExpeditionStatus.Completed;
			}
		}
	}
}