import { ObjStatus, Expedition } from "../models/expedition";

export abstract class Objective {
	type: TaskType;
	skill: SkillType;
	amount: number;
	id:number;
	itemName?:string;

	constructor(type:TaskType) {
		this.type = type;
	}

	abstract startTurn(expedition:Expedition, status:ObjStatus):void;

	get icon():string{
		return skillMap[this.skill].icon;
	}

	get isChance():boolean{
		return this.type == TaskType.Trap || this.type == TaskType.Chance;
	}

	get display():string|number{
		if(this.type == TaskType.Chance){
			return Math.round(this.amount*100) + '%'
		}else if(this.type == TaskType.Requirement){
			return 'Required';
		}

		return this.amount;
	}

	setStatus(expedition:Expedition, status:ObjStatus){
		status.countDown = this.amount >= 1 ? this.amount : 1;
		if(this.skill){
			status.countDown -= expedition.getSkill(this.skill);
			if(status.countDown <= 0){
				status.status = ExpeditionStatus.Completed;
			}
		}
	}
}