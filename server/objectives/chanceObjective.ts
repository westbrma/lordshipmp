import { Expedition, ObjStatus } from "../models/expedition";
import { helper } from "../helper";
import { Objective } from "./objective";

export class ChanceObjective extends Objective {
	constructor(skill: SkillType, amount: number){
		super(TaskType.Chance);
		this.skill = skill;
		this.amount = amount;
	}

	getChance(exp: Expedition):number{
		var stat = 0;
		exp.cards.forEach(u => {
			stat += u.getSkill(this.skill);
		});
		stat *= .1;

		if(exp.lucky){
			stat += .25;
		}

		var chance = this.amount + stat;
		if(chance > 1){
			chance = 1;
		}
		return chance;
	}

	startTurn(exp: Expedition, status:ObjStatus) {
		var chance = this.getChance(exp);
		var pass = (chance >= 1) || helper.getRandomChance(chance);

		if (pass) {
			status.status = ExpeditionStatus.Completed;
		} else {
			status.status = ExpeditionStatus.Failed;
		}
	}
}