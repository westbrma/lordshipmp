import { Objective } from "./objective";
import { Expedition, ObjStatus } from "../models/expedition";

export class Requirement extends Objective {
	constructor(itemName:string){
		super(TaskType.Requirement);
		this.itemName = itemName;
	}

	get icon():string{
		return cardMap[this.itemName].icon;
	}

	startTurn(exp: Expedition, status:ObjStatus) {
		var item = exp.cards.find(c => c.name == this.itemName);
		if(item != null){
			item.destroy();
			status.status = ExpeditionStatus.Completed;
		}else{
			status.status = ExpeditionStatus.Failed;
		}
	}
}