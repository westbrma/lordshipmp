import { MapObject } from "../models/mapObject";
import { Tile } from "../models/tile";
import { Board } from "../models/board";
import { PlayerArmy } from "../mapObjects/playerArmy";

export class UpdateGame {
	type: MessageType.UpdateGame;
	mapObjects: MapObject[] = [];
	tileUpdates: Tile[] = [];
	board?: Board;
	deletedIds:number[] = [];

	toJSON(army:PlayerArmy): IUpdateGame {
		var result: IUpdateGame = {
			type: MessageType.UpdateGame,
			mapObjects: [],
			tileUpdates: this.tileUpdates.length > 0 ? [] : undefined,
			board: this.board ? this.board.toJSON() : undefined,
			myInfo: army.getMyInfo(),
			deletedIds: this.deletedIds.length > 0 ? this.deletedIds : undefined,
			start: this.board ? true : undefined
		}

		//filter(o => army.tileMap.isTileVisible(o.tile))
		this.mapObjects.forEach(o => {
			if(!o.destroyed){
				result.mapObjects.push(o.toJSON(o.team == army));
			}
		});

		this.tileUpdates.forEach(t => {
			result.tileUpdates.push({
				x: t.x,
				y: t.y,
				cityId: t.city ? t.city.id : undefined,
				occupantId: t.occupant ? t.occupant.id : undefined
			})
		});

		return result;
	}
}