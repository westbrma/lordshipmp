let _lastID = 0;

export let helper = {

	genID: function (): number {
		_lastID++;
		return _lastID;
	},

	removeFromArray: function (item: any, array: any[]) {
		var idx = array.indexOf(item);
		if (idx >= 0) {
			array.splice(idx, 1);
		}
	},
	
	shuffleArray:function(a:any[]) {
		 for (let i = a.length - 1; i > 0; i--) {
			  const j = Math.floor(Math.random() * (i + 1));
			  [a[i], a[j]] = [a[j], a[i]];
		 }
		 return a;
	},
	
	lerp:function(value1, value2, amount) {
		amount = amount < 0 ? 0 : amount;
		amount = amount > 1 ? 1 : amount;
		return value1 + (value2 - value1) * amount;
	},
	
	getRandomMapItem:function<T>(obj) {
		 var result;
		 var count = 0;
		 for (var prop in obj)
			  if (Math.random() < 1/++count)
				  result = prop;
		 return result;
	},
	
	getRandomItem:function<T>(arg: T[]): T {
		if (arg.length == 0) return null;
		return arg[helper.getRandomInt(0, arg.length - 1)];
	},
	
	getRandomInt:function(min, max):number {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	
	getRandomChance:function(chance: number): boolean {
		if (chance <= 0 || chance >= 1) {
			throw new Error("Invalid RandomChance parameter value");
		}
	
		return Math.random() <= chance;
	},
	
	getRandom:function(min, max):number {
		return Math.random() * (max - min) + min;
	},
	
	clearSelection:function() {
		if (window.getSelection) {
			if (window.getSelection().empty) {  // Chrome
				window.getSelection().empty();
			} else if (window.getSelection().removeAllRanges) {  // Firefox
				window.getSelection().removeAllRanges();
			}
		}
	},
	
	angleToVector:function(angle: number): Position2 {
		return new Position2(Math.cos(angle), Math.sin(angle));
	},
	
	vectorToAngle:function(vector: Position2): number {
		return Math.atan2(vector.y, vector.x);
	},
}