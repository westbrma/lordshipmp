type IconMap = {[key:string]:string[]}; 
var landIcons:IconMap = {}

landIcons[ObjType.Animal] = ['images/animal.png'];
landIcons[ObjType.Chest] = ['images/chest.png'];
landIcons[ObjType.City] = ['images/city.png'];
landIcons[ObjType.Creep] = ['images/bounty.png','images/bounty2.png'];
landIcons[ObjType.Discovery] = ['images/wonder.png'];
landIcons[ObjType.Army] = ['images/army.png'];
landIcons[ObjType.Merchant] = ['images/merchant.png'];
landIcons[ObjType.Recruit] = ['images/recruit.png'];
landIcons[ObjType.Expedition] = ['images/party.png'];
landIcons[ObjType.Posting] = ['images/mission.png'];

var waterIcons:IconMap = {}
waterIcons[ObjType.Army] = ['images/armyBoat.png'];
waterIcons[ObjType.Discovery] = ['images/waterMission.png'];
waterIcons[ObjType.Expedition] = ['images/armyBoat.png'];
waterIcons[ObjType.Animal] = ['images/fish.png'];
waterIcons[ObjType.Creep] = ['images/bountySea.png'];

function setObjectIcon(obj:MapObject){
	let icons:string[] = null;
	if(obj.tile.type == TileType.Water){
		icons = waterIcons[obj.type];
	}

	if(!icons){
		icons = landIcons[obj.type];
	}
	obj.setIcon(getRandomItem(icons));
}