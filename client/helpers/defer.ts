class Defer<T> {
	promise: Promise<T>;
	_resolve: Function;
	_reject: Function;

	constructor() {
		this.promise = new Promise((resolve, reject) => {
			this._resolve = resolve;
			this._reject = reject;
		});
	}

	return(success, data) {
		if (success) {
			this._resolve(data);
		} else {
			this._reject();
		}
	}
}