function makeReactive(obj:any) {
	for(let prop in obj){
		if(Object.getOwnPropertyDescriptor(obj, prop).get === undefined){
			var value = obj[prop];
			delete obj[prop];
			Vue.set(obj, prop, value);
		}
	}
}