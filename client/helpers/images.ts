var whiteColor = parseInt("ffffff", 16);
var genImageMap: { [key: string]: HTMLImageElement } = {};

function getColorImageKey(src: string, color: string):string{
	return src + '_' + color;
}

function getColorImageURL(src: string, color: string): string {
	var key = src + '_' + color;
	if (genImageMap[key] === undefined) {
		var imageObj: HTMLImageElement = genImageMap[src];
		var canvas = document.createElement('canvas');
		canvas.width = imageObj.width;
		canvas.height = imageObj.height;
		var context = canvas.getContext('2d');
		context.drawImage(imageObj, 0, 0);
		replaceColor(context, color);

		let newImage = new Image();
		newImage.src = canvas.toDataURL("image/png", 1);
		genImageMap[key] = newImage;
	}

	return key;
}

interface IRGB {
	r: number;
	g: number;
	b: number;
}

function randomRGB(): string {
	var rgb = {
		r: getRandomInt(0, 255),
		g: getRandomInt(0, 255),
		b: getRandomInt(0, 255)
	}

	return rgbToHex(rgb.r, rgb.g, rgb.b);
}

function componentToHex(c) {
	var hex = c.toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function hexToRgb(hex: string): IRGB {
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	} : null;
}

function darkenRgb(hex: string, percent: number) {
	var rgb = hexToRgb(hex);
	rgb.r = Math.floor(rgb.r * percent);
	rgb.g = Math.floor(rgb.g * percent);
	rgb.b = Math.floor(rgb.b * percent);
	return rgbToHex(rgb.r, rgb.g, rgb.b);
}

function replaceColor(context: CanvasRenderingContext2D, colorStr) {
	var newColor = hexToRgb(colorStr);
	var imageData = context.getImageData(0, 0, context.canvas.width, context.canvas.height);
	var data = imageData.data;
	for (var i = 0; i < data.length; i += 4) {
		var r = data[i];
		var g = data[i + 1];
		var b = data[i + 2];
		var a = data[i + 3];

		if (a == 255 && r == 255 && g == 255 && b == 255) {
			data[i] = newColor.r;
			data[i + 1] = newColor.g;
			data[i + 2] = newColor.b;
		}
	}
	context.putImageData(imageData, 0, 0);
}

function buildNumber(num:number):HTMLImageElement{
	let digits = num.toString();
	let images = [];
	let h = 0;
	let w = 0;
	for(let i=0;i<digits.length; i++){
		let imgUrl = `images/numbers/${digits[i]}.png`;
		let img = genImageMap[imgUrl];
		images.push(img);
		w += img.width;
		if(img.height > h){
			h = img.height;
		}
	}

	let canvas = document.createElement('canvas');
	canvas.width = w;
	canvas.height = h;
	let x = 0;
	let ctx = canvas.getContext('2d');
	for(let i=0; i<images.length; i++){
		let img = images[i];
		ctx.drawImage(img, x, 0);
		x += img.width - 1;
	}

	let img = new Image();
	img.src = canvas.toDataURL();
	return img;
}

function generateNumberIcons() {
	colors.forEach(t => {
		getColorImageURL('images/tower.png', t);
		getColorImageURL('images/city.png', t);
		getColorImageURL('images/party.png', t);
		getColorImageURL('images/army.png', t);
		getColorImageURL('images/armyBoat.png', t);
		getColorImageURL('images/scoreBG.png', t);

		for (let i = 1; i < 50; i++) {
			getColorImageURL(`number:${i}.png`, t);
		}
	});

	for (let i = 1; i < 50; i++) {
		getColorImageURL(`number:${i}.png`, NPC_COLOR);
	}
}