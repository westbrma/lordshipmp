if(config.isProd){
	//gtag('config', 'UA-114354000-1');
}

enum MetricAction {
	NewGameStarted = 'NewGameStarted',
	Rounds = 'Rounds',
	Help = 'Help'
}

function trackRounds() {
	if(state.rounds > 0){
		trackEvent(MetricAction.Rounds, state.rounds);
	}
}

function trackEvent(action: MetricAction, data?:any) {
	if (!config.isProd) return;

	// gtag('event', action, {
	// 	'event_category': 'game',
	// 	'value' : data
	// });
}