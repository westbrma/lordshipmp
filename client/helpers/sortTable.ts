var SortTable = function (el, list, callback?):void {
	var table = $(el);
	var headers = table.find('th');
	var sortedHeader;
	var direction = 0;
	var prop = null;

	function deep_value(obj, path){
		path=path.split('.');
		for (var i=0; i<path.length; i++){
			obj = obj[path[i]];
		}
		return obj;
	}

	headers.each(function (index, header:any) {
		header = $(header);
		let attr = header.attr('sortprop');
		if (attr) {
			header.click(onHeaderClick);
		}
	});

	function sortAlpha(a, b) {
		var aValue = deep_value(a,prop);
		var bValue = deep_value(b,prop);
		if (aValue < bValue)
			return -direction;
		if (aValue > bValue)
			return direction;
		return 0;
	}

	function sortDate(a, b) {
		var aValue = deep_value(a,prop);
		var bValue = deep_value(b,prop);

		if (aValue < bValue)
			return -direction;
		if (aValue > bValue)
			return direction;
		return 0;
	}

	function sortNumeric(a, b) {
		var aValue = deep_value(a,prop);
		var bValue = deep_value(b,prop);

		if (aValue.toLowerCase) {
			aValue = parseFloat(aValue.replace(/[^0-9 | ^.]/g, ''));
			bValue = parseFloat(bValue.replace(/[^0-9 | ^.]/g, ''));
		}

		if (aValue < bValue)
			return -direction;
		if (aValue > bValue)
			return direction;
		return 0;
	}

	function setList(newList){
		if(list != newList){
			list = newList;
			sort(sortedHeader, direction);
		}
	}

	function sort(header, dir){
		if (sortedHeader != null) {
			$(sortedHeader).removeClass('ascending descending');
		}

		direction = dir;
		sortedHeader = header;
		if(header == null){
			return;
		}

		header = $(header);
		if (direction > 0) {
			header.addClass('ascending');
		} else {
			header.addClass('descending');
		}

		prop = header.attr('sortprop');

		if (callback && callback(prop, direction)) {
			return;
		}

		var sortFunc = sortAlpha;
		if (header.attr('sortNumeric') != undefined) {
			sortFunc = sortNumeric;
		} else if (header.attr('sortDate') != undefined) {
			sortFunc = sortDate;
		}

		list.sort(sortFunc);
	}

	function onHeaderClick() {
		let dir = this == sortedHeader ? -direction : 1;
		sort(this, dir);
	}

	this.setList = setList;
	this.sort = sort;
}