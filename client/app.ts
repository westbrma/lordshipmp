enum AppEvent {
	GameDestroyed = "GameDestroyed"
}

@VueComponent()
class App extends Vue {
	state: State = state;

	mounted() {
		handler.initWindowEvents();
	}

	toggleFullscreen() {
		toggleFullscreen();
	}

	fireEvent(event: AppEvent, args?: any) {
		this.$emit(event, args);
	}
}

var app: App = null;

$(document).ready(function () {
	let p1 = api.init();
	let p2 = LoadingDlg.open();

	Promise.all([p1, p2]).then(() => {
		app = new App();
		app.$mount('#app');
		MainLobbyDlg.open();
	});
});

window.onerror = function (message: string, source, lineno, colno, error) {
	ErrorDlg.open([message]);
}

window.addEventListener("unhandledrejection", function (err: PromiseRejectionEvent) {
	if (err && err.reason) {
		ErrorDlg.open([err.reason.message]);
	}
});

function toggleFullscreen() {
	let doc:any= document;

	var isInFullScreen = (doc.fullscreenElement && doc.fullscreenElement !== null) ||
		(doc.webkitFullscreenElement && doc.webkitFullscreenElement !== null);

	if (isInFullScreen) {
		if (doc.exitFullscreen) {
			doc.exitFullscreen();
		} else if (doc.webkitExitFullscreen) {
			doc.webkitExitFullscreen();
		} else if (doc.mozCancelFullScreen) {
			doc.mozCancelFullScreen();
		} else if (doc.msExitFullscreen) {
			doc.msExitFullscreen();
		}
	} else {
		var docElm: any = document.documentElement;
		if (docElm.requestFullscreen) {
			docElm.requestFullscreen();
		} else if (docElm.mozRequestFullScreen) {
			docElm.mozRequestFullScreen();
		} else if (docElm.webkitRequestFullScreen) {
			docElm.webkitRequestFullScreen();
		} else if (docElm.msRequestFullscreen) {
			docElm.msRequestFullscreen();
		}
	}
}