function Sortable(el, binding, vnode) {
    el.sortable = this;
    var list = [];
    var debug = false;
    var id = Math.floor(Math.random() * 100);
    logMessage('sortable()');
    
    var dragElement = null;
    var origIndex = 0;
    var newIndex = 0;
    var dropped = false;

    function logMessage(message){
        if(debug){
            console.log(id+': '+message);
        }
    }

    doUpdate(binding, vnode);
    this.update = doUpdate;

    function doUpdate(binding, vnode) {
        if (list != binding.value) {
            list = binding.value;

            var children = el.children;
            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                child.draggable = true;
                child.ondragstart = dragStart;
                child.ondragenter = dragEnter;
                child.ondragover = dragOver;
                child.ondragend = dragEnd;
                child.ondrop = drop;
            }
        }
    }

    function dragStart(event) {
        dropped = false;
        dragElement = event.target;
        origIndex = newIndex = getTargetIndex(dragElement);
        event.dataTransfer.setData('Text', this.id);

        logMessage('dragStart(): ' + origIndex);
        event.cancelBubble = true;
    }

    function getTargetIndex(target){
        var result = -1;
        var cur = target;
        while(cur){
            if(cur.parentElement == el){
                result = Array.prototype.indexOf.call(el.children, cur);
                break;
            }else{
                cur = cur.parentElement;
            }
        }
        return result;
    }

    function dragOver(event){
        if (dragElement && getTargetIndex(event.target) >= 0) {
            event.preventDefault();
        }
    }

    function dragEnter(event) {
        var targetIndex = getTargetIndex(event.target);
        if (dragElement && targetIndex >= 0) {
            logMessage('dragEnter(): ' + targetIndex);
            moveItem(newIndex, targetIndex);
            newIndex = targetIndex;
            event.preventDefault();
        }
    }

    function moveItem(fromIndex, toIndex) {
        if (fromIndex != toIndex) {
            var item = list[fromIndex];
            list.splice(fromIndex, 1);
            list.splice(toIndex, 0, item);
            if(vnode.context.onItemMoved){
                vnode.context.onItemMoved(list, fromIndex, toIndex);
            }

            Vue.nextTick(function(){
                $('.dragTarget').removeClass('dragTarget');
                $(el.children[newIndex]).addClass('dragTarget');
            });
        }
    }

    function drop(ev) {
        if (dragElement) {
            dropped = true;
        }
        logMessage('drop()');
    }

    function dragEnd(event) {
        if (dragElement) {
            if (!dropped) {
                moveItem(newIndex, origIndex);
            }
            dragElement = null;
            $('.dragTarget').removeClass('dragTarget');
        }
    }
}

Vue.directive("sortable", {
    bind: function (el, binding, vnode) {
        new Sortable(el, binding, vnode);
    },
    componentUpdated: function (el, binding, vnode) {
        if(el.sortable){
            el.sortable.update(binding, vnode);
        }else{
            new Sortable(el, binding, vnode);
        }
    }
});