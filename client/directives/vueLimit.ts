class VueLimit{
	constructor(el: HTMLInputElement, binding, vnode){
		var prop = binding.value.prop;
		var min = binding.value.min;
		var max = binding.value.max;
		var context = vnode.context;
		el.onchange = function (input) {
			var val = parseInt(el.value);
			if(isNaN(val)) context[prop] = min;
			else if (val < min) context[prop] = min;
			else if (val > max) context[prop] = max;
		}
	}
}

Vue.directive("limit", {
	bind: function (el: HTMLInputElement, binding, vnode) {
		new VueLimit(el, binding, vnode);
	},
	unbind: function (el, binding, vnode) {
		el.onchange = null;
	}
});