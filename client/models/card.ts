class Card {
	id:number = 0;
	selected:boolean = false;
	onMission:boolean = false;
	army:Army = null;
	def:ICardDef = null;

	constructor(card:ICard){
		this.id = card.id;
		this.def = cardMap[card.name];
		this.onMission = card.onMission;
		
		if(!this.def){
			this.def = unitMap[card.name];
		}
	}

	get name(){
		return this.def.name;
	}

	get info():string{
		if(this.def.info){
			return this.def.name + ': ' + this.def.info;
		}else{
			return this.def.name;
		}
	}

	get skills():SkillLevelMap{
		return this.def.skills;
	}

	get hp(){
		if(!this.def.isUnit) return 0;
		return 3;
	}

	getSkill(type:SkillType){
		return (this.skills && this.skills[type]) ? this.skills[type] : 0;
	}

	destroy(){
		if(this.army){
			this.army.removeCard(this);
		}
	}
}