interface ISkillLevel2 {
	skill:SkillType,
	count:number,
	def:ISkillDef
}

class SkillSummary {
	items: ISkillLevel2[] = [];

	set(map: SkillLevelMap) {
		this.items.length = 0;
		for (let prop in map) {
			this.items.push({
				skill: <SkillType>prop,
				count: map[prop],
				def: skillMap[prop]
			});
		}

		this.items.sort((a, b) => {
			var x = a.skill;
			var y = b.skill;
			return x < y ? -1 : x > y ? 1 : 0;
		});
	}

	reset() {
		this.items = [];
	}
}

class Expedition {
	army: Army = null;
	cards: Card[] = [];
	target:MapObject = null;
	hp: number = 0;
	overtake: number = 0;
	skills: SkillSummary = new SkillSummary();

	constructor(army: Army, target: MapObject) {
		this.army = army;
		this.target = target;
	}

	addCard(unit: Card) {
		this.cards.push(unit);
		this.updateSkillSummary();
	}

	removeCard(unit: Card) {
		removeFromArray(unit, this.cards);
		this.updateSkillSummary();
	}

	validate(): boolean {
		var alreadyInExp: boolean = false;
		this.calcHP();

		this.cards.forEach(c => {
			if (c.onMission) {
				alreadyInExp = true;
			}
		});

		if (alreadyInExp) {
			info.setMessage('Cannot send units already in expedition');
			return false;
		}
		else if (this.hp <= 0) {
			info.setMessage('Select units to send');
			return false;
		}

		return true;
	}

	updateSkillSummary() {
		var map: SkillLevelMap = {};
		var hp = 0;

		this.cards.forEach(u => {
			hp += u.hp;
			for (let prop in u.skills) {
				let cur = map[prop];
				if (cur === undefined) cur = 0;
				map[prop] = cur + u.def.skills[prop];
			}
		});

		this.hp = hp;
		this.skills.set(map);
	}

	calcHP() {
		this.hp = 0;
		this.cards.forEach(m => {
			if (m.def.isUnit) {
				this.hp += m.hp
			}
		});
	}

	send() {
		this.cards.forEach(c => {
			c.onMission = true;
		});

		state.turn.startExpedition(this.cards, this.target);
	}

	hasItem(type: ItemType) {
		return this.cards.find(c => { return c.name == type }) != null;
	}

	getSkill(type: SkillType) {
		var result = 0;
		this.cards.forEach(u => {
			result += u.getSkill(type);
		});

		return result;
	}
}