class Game implements IGame {
	id:number = 0;
	active:boolean = false;
	players:IPlayer[] = [];
	maxPlayers:number = 0;
	topScore:number = 0;
	turnPlayer:IPlayer = null;
	ownerId: number = 0;
	grid:string[][] = [];

	//vm
	selected:boolean = false;

	populate(g:IGame){
		this.id = g.id;
		this.active = g.active;
		this.maxPlayers = g.maxPlayers;
		this.ownerId = g.ownerId;
	}

	get myTurn():boolean {
		return this.turnPlayer && this.turnPlayer.id == state.player.id;
	}
}