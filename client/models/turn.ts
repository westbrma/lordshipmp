class Turn implements ITakeTurn {
	type: MessageType = MessageType.TakeTurn
	move?: ICoord = null;
	cityUpdates: ICityUpdate[] = [];
	newExpeditions: INewExpedition[] = [];

	moveArmy(tile:Tile){
		this.move = {
			x: tile.x,
			y: tile.y
		}
	}

	getCityUpdate(city:City):ICityUpdate{
		var u = state.turn.cityUpdates.find(cu => cu.cityId == city.id);
		if (u == null) {
			u = {
				cityId: city.id,
				expansions: []
			}
			state.turn.cityUpdates.push(u);
		}
		return u;
	}

	startExpedition(cards:Card[], target:MapObject){
		var expedition:INewExpedition = {
			cardIds:[],
			objectId:target.id
		}

		cards.forEach(c => {
			expedition.cardIds.push(c.id);
		});

		this.newExpeditions.push(expedition);
		target.pendingExpedition = true;
		state.pendingTargets.push(target);
		target.tile.render();
	}

	reset() {
		this.move = null;
		this.cityUpdates = [];
		this.newExpeditions = [];
	}
}