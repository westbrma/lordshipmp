enum AnimationType {
	None,
	BlinkObj,
	BlinkTile,
	Fade
}

class TAnimation {
	type:AnimationType;
	blink:boolean;
	useAlt:boolean;
	alpha:number;

	// time in milliseconds
	duration:number = 0;
	next:number = 0;
	step:number = 0;
	tile:Tile;

	constructor(tile:Tile, type:AnimationType, duration:number, step:number){
		this.type = type;
		this.tile = tile;
		this.duration = duration;
		this.step = step;
	}
}

class Tile {
	x: number;
	y: number;
	occupant: MapObject = null;
	type: TileType;
	city: City = null;
	visibility: TileVis = TileVis.Hidden;
	_adj: Tile[] = null;
	$el?: HTMLElement;
	hover: boolean = false;
	def: TileDef = null;
	image:HTMLImageElement;
	altImage:HTMLImageElement;
	animation:TAnimation;

	constructor(x: number, y: number, type: TileType) {
		this.x = x;
		this.y = y;
		this.type = type;
		this.def = tileDefs.getDef(this.type);
		let idx = getRandomInt(0, this.def.images.length - 1);
		let icon = this.def.icons[idx];
		if(icon.indexOf('_alt') >= 0){
			icon = icon.replace('_alt', '');
			idx = this.def.icons.indexOf(icon);
		}

		this.image = this.def.images[idx];
		let altSrc = `${icon}_alt`;
		let altIdx = this.def.icons.indexOf(altSrc);
		if(altIdx >= 0){
			this.altImage = this.def.images[altIdx];
		}
	}

	get team(): Army {
		return this.city ? this.city.team : null;
	}

	get isVisible() {
		return this.visibility == TileVis.Visible;
	}

	get landTile(): boolean {
		return this.def.Type != TileType.Water;
	}

	get west() {
		return board.tile(this.x - 1, this.y);
	}

	get east() {
		return board.tile(this.x + 1, this.y);
	}

	get north() {
		return board.tile(this.x, this.y - 1);
	}

	get south() {
		return board.tile(this.x, this.y + 1);
	}

	get adjacent() {
		if (this._adj == null) {
			var adj = [];
			if (this.west) adj.push(this.west);
			if (this.east) adj.push(this.east);
			if (this.south) adj.push(this.south);
			if (this.north) adj.push(this.north);
			this._adj = adj;
		}

		return this._adj;
	}

	isAdjacent(other: Tile): boolean {
		return this.adjacent.indexOf(other) >= 0;
	}

	getRandomNeighbor(open: boolean = true, notControlled: boolean = false): Tile {
		var adj = this.adjacent.slice(0);
		while (adj.length > 0) {
			var idx = getRandomInt(0, adj.length - 1);
			var tile = adj[idx];
			if ((!open || tile.occupant == null) && (!notControlled || tile.city == null)) {
				return tile;
			} else {
				adj.splice(idx, 1);
			}
		}

		return null;
	}

	setVisibility(vis: TileVis) {
		if (vis != this.visibility) {
			if (this.visibility == TileVis.Hidden) {
				let a = new TAnimation(this, AnimationType.Fade, 500, 100);
				a.alpha = .8;
				board.setAnimation(this, a);
			} else if (vis == TileVis.Fog && this.animation) {
				delete this.animation;
			}

			this.visibility = vis;
		}

		this.render();
	}

	setControlledBy(city: City) {
		this.city = city;
		this.render();
	}

	getRawDistanceFrom(fromTile: Tile): number {
		let xDist = Math.abs(this.x - fromTile.x);
		let yDist = Math.abs(this.y - fromTile.y);
		return xDist + yDist;
	}

	getDistanceFrom(fromTile: Tile): number {
		let xDist = Math.abs(this.x - fromTile.x);
		let yDist = Math.abs(this.y - fromTile.y);
		var tt = xDist + yDist;
		return Math.ceil(tt / MaxMove);
	}

	setOccupant(obj: MapObject) {
		this.occupant = obj;
		this.render();
	}

	render() {
		board.renderTile(this);
	}

	getOffset(): Position2 {
		let result = new Position2(this.x * handler.tileSize, this.y * handler.tileSize);
		result.x += handler.scrollPos.x;
		result.y += handler.scrollPos.y;
		return result;
	}
}