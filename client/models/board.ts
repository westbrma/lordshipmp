var board: Board = null;
const _hoverColor = '#1E1E1E';
const _hoverFog = '#1E1E1E';
const _tileBG = '#000000';
const _fogBG = '#111111';
const BOARD_ANIMATION_MS = 100;
const ALT_IMAGE_UPDATE_TIME = 2000;

class Board {
	width: number;
	height: number;
	tiles: Tile[][] = [];
	_pendingRender = new Map<Tile,boolean>();
	animations:Tile[] = [];
	_interval;
	_nextAltImageUpdate = ALT_IMAGE_UPDATE_TIME;

	constructor(b:IBoard){
		board = this;
		this.width = b.width;
		this.height = b.height;

		for (let y = 0; y < b.tiles.length; y++) {
			var r = b.tiles[y];
			var row: Tile[] = [];
			this.tiles.push(row);
			for (let x = 0; x < r.length; x++) {
				let t = r[x];
				row.push(new Tile(x, y, t));
			}
		}

		this._interval = setInterval(this.renderAnimations.bind(this), BOARD_ANIMATION_MS);
	}

	renderAnimations(){
		if(!state.gameActive){
			clearInterval(this._interval);
		}

		this._nextAltImageUpdate -= BOARD_ANIMATION_MS;
		if(this._nextAltImageUpdate <= 0){
			this._nextAltImageUpdate = ALT_IMAGE_UPDATE_TIME;
			this.setAltTiles();
		}

		for(let i=this.animations.length-1; i>=0; i--){
			let t = this.animations[i];
			let a = t.animation;
			if(a == undefined){
				this.animations.splice(i, 1);
				continue;
			}

			a.next -= BOARD_ANIMATION_MS;
			if(a.next > 0) continue;

			a.next = a.step;
			a.duration -= a.step;
			if(a.duration <= 0){
				delete t.animation;
				this.animations.splice(i, 1);
			}
			this._renderTile(t);
			
			if (a.type == AnimationType.BlinkTile) {
				a.useAlt = !a.useAlt;
			} else if (a.type == AnimationType.Fade) {
				a.alpha -= .2;
			} else if (a.type == AnimationType.BlinkObj) {
				a.blink = !a.blink;
			}
		}
	}

	setAnimation(tile:Tile, animation:TAnimation){
		if(tile.animation == null){
			this.animations.push(tile);
		}
		tile.animation = animation;
	}

	setAltTiles(){
		for(let i=0; i<10; i++){
			let t = getRandomItem(state.visTiles);
			if(t.altImage != null && t.animation == null){
				t.animation= new TAnimation(t, AnimationType.BlinkTile, 5000, 500);
				t.animation.useAlt = true;
				this.animations.push(t);
			}
		}
	}

	get wPixels():number{
		return TILE_SIZE * this.width;
	}

	get yPixels():number{
		return TILE_SIZE * this.height;
	}

	tile(x: number, y: number) {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
			return null;
		}
		return this.tiles[y][x];
	}

	scrollToTile(tile:Tile){
		var pos:Position2 = new Position2(tile.x * handler.tileSize, tile.y * handler.tileSize);
		pos.x += handler.tileSize/2;
		pos.y += handler.tileSize/2;
		handler.centerWorldPos(pos);
	}

	exposeAll(){
		this.tiles.forEach(r =>{
			r.forEach(t=>{
				t.setVisibility(TileVis.Visible);
			});
		})
	}

	hideAll(){
		this.tiles.forEach(r =>{
			r.forEach(t=>{
				t.setVisibility(TileVis.Hidden);
			});
		})
	}

	doRefresh: boolean = false;
	ctx: CanvasRenderingContext2D;

	refresh() {
		if (this.doRefresh) return;

		this.doRefresh = true;

		Vue.nextTick(() => {
			this._renderTiles();
			this.doRefresh = false;
		})
	}

	renderTile(tile:Tile){
		this._pendingRender.set(tile, true);
		Promise.resolve().then(r => {
			this._pendingRender.forEach((value,key) => {
				this._renderTile(key);
			});
			this._pendingRender.clear();
		});
	}

	_renderTile(tile: Tile) {
		let ctx = this.ctx;
		if (!ctx) return;

		let a = tile.animation;
		let aType = a ? a.type : AnimationType.None;

		ctx.globalAlpha = 1;

		let xPos = tile.x * TILE_SIZE;
		let yPos = tile.y * TILE_SIZE;

		let fog = tile.visibility == TileVis.Fog;

		if (state.overTile == tile) {
			ctx.fillStyle = fog ? _hoverFog : _hoverColor;
		}else{
			ctx.fillStyle = fog ? _fogBG : _tileBG;
		}
		
		ctx.fillRect(xPos, yPos, TILE_SIZE, TILE_SIZE);

		if (tile.visibility == TileVis.Hidden) {
			return;
		}

		if (fog) {
			ctx.globalAlpha = .3;
		}

		function drawTeamBorder() {
			ctx.strokeStyle = tile.city.team.color;
			ctx.globalAlpha = .3;
			ctx.lineWidth = 2;
			let half = 1;
			let bottom = yPos + TILE_SIZE - half;
			let right = xPos + TILE_SIZE - half;
			let left = xPos + half;
			let top = yPos + half;

			ctx.beginPath();

			if (!tile.north || tile.north.city != tile.city) {
				ctx.moveTo(left, top);
				ctx.lineTo(right, top);
			}

			if (!tile.south || tile.south.city != tile.city) {
				ctx.moveTo(left, bottom);
				ctx.lineTo(right, bottom);
			}

			if (!tile.west || tile.west.city != tile.city) {
				ctx.moveTo(left, top);
				ctx.lineTo(left, bottom);
			}

			if (!tile.east || tile.east.city != tile.city) {
				ctx.moveTo(right, top);
				ctx.lineTo(right, bottom);
			}

			ctx.stroke();
			ctx.closePath();
			ctx.globalAlpha = fog ? .5 : 1;
		}

		if (tile.city != null && !fog) {
			drawTeamBorder();
		}

		ctx.drawImage(a && a.useAlt ? tile.altImage : tile.image, xPos, yPos, TILE_SIZE, TILE_SIZE);

		if (tile.occupant != null && tile.visibility == TileVis.Visible) {
			if(!a || !a.blink){
				let img = genImageMap[tile.occupant.icon];
				ctx.drawImage(img, xPos, yPos, TILE_SIZE, TILE_SIZE);
			}
			
			let obj = tile.occupant;

			if (obj.hp > 0) {
				let icon = obj.hpIcon;
				ctx.drawImage(icon, xPos, yPos + TILE_SIZE - icon.height);
			}

			if (obj.type == ObjType.Expedition) {
				if (obj.travelRemaing > 0) {
					let icon = genImageMap['images/travelIcon.png'];
					ctx.drawImage(icon, xPos + TILE_SIZE - icon.width, yPos + TILE_SIZE - icon.height)
				} else if (obj.eta > 0) {
					let icon = genImageMap[`number:${obj.eta}.png`];
					ctx.drawImage(icon, xPos + TILE_SIZE - icon.width, yPos + TILE_SIZE - icon.height)
				}
			}

			if(tile.occupant.pendingExpedition){
				let icon = genImageMap['images/pendingExp.png'];
				ctx.drawImage(icon, xPos, yPos);
			}
		}

		if(aType == AnimationType.Fade){
			ctx.globalAlpha = a.alpha;
			ctx.fillStyle = _tileBG;
			ctx.fillRect(xPos, yPos, TILE_SIZE, TILE_SIZE);
		}
	}

	_renderTiles() {
		let canvas = <HTMLCanvasElement>document.querySelector('#board');
		canvas.width = board.wPixels;
		canvas.height = board.yPixels;

		let ctx = this.ctx = canvas.getContext('2d');
		ctx.globalAlpha = 1;
		ctx.imageSmoothingEnabled = false;
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		for (let y = 0; y < board.tiles.length; y++) {
			let row = board.tiles[y];
			for (let x = 0; x < row.length; x++) {
				this._renderTile(row[x]);
			}
		}
	}
}