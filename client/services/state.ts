class State {
	game: Game = null;
	games: Game[] = [];
	messages: string[] = [];
	player: IPlayer = null;
	armies: Army[] = [];
	playerArmy: Army = null;
	mapWidth = 30;
	mapHeight = 15;
	overTile: Tile = null;
	mapObjects: { [key: number]: MapObject } = {};
	pendingTargets:MapObject[] = [];
	_selected: MapObject = null;
	winScore = 100;
	dragging: boolean = false;
	aiTeamCount: number = 10;
	rounds: number = 0;
	debugging: boolean = false;
	debugArmy: Army = null;
	waiting: boolean = null;
	turn: Turn = null
	visTiles: Tile[] = [];
	socketAlive = false;
	mouseHL: boolean = false;
	expanding:boolean = false;

	constructor() {
		this.player = {
			name: settings.get<string>(SettingType.playerName,''),
			id: 0,
			color: colors[0]
		}
	}

	get isPlayerTurn() {
		return !this.waiting;
	}

	get selected() {
		return this._selected;
	}

	getNextColor(): string {
		let color: string = null;
		for (let i = 0; i < colors.length; i++) {
			color = colors[i];
			if (!this.armies.find(a => a.color == color)) {
				break;
			}
		}
		return color;
	}

	get gameActive(): boolean {
		return this.game && this.game.active;
	}

	set selected(value: MapObject) {
		if (this._selected) {
			this._selected.selected = false;
		}

		this._selected = value;
		if (value) {
			value.selected = true;
			if (value == this.playerArmy) {
				setTimeout(() => {
					this.selected = null;
				}, 500);
			}
		}
	}

	get city(): City {
		if (this._selected != null && this._selected.type == ObjType.City && this._selected.team == this.playerArmy) {
			return <City>this._selected;
		}

		return null;
	}

	setOverTile(tile: Tile, force?: boolean) {
		if (tile == this.overTile && !force) {
			return;
		}

		let prev = this.overTile;
		this.overTile = tile;
		this.mouseHL = tile && tile.occupant != null;

		if (tile) {
			board.renderTile(tile);
		}

		if (prev) {
			board.renderTile(prev);
		}

		eventBus.fireEvent(Events.overTileChanged, tile);
	}

	reset() {
		this.game = null;
		this.armies = [];
		this.mapObjects = {};
		this.messages.length = 0;
		this.debugging = false;
		this.playerArmy = null;
		this.rounds = 0;
	}

	initBoard(newBoard: IBoard) {
		new Board(newBoard);
	}

	start() {
		handler.setTeamStyles();
		this.turn = new Turn();

		this.setOverTile(this.playerArmy.tile);

		Vue.nextTick(() => {
			handler.setSize(DEFAULT_SIZE);
			board.scrollToTile(this.playerArmy.tile);
		});

		trackEvent(MetricAction.NewGameStarted);
		board.refresh();
	}

	gameOver() {
		gameOverDlg.open(false);
	}

	buildCity() {

	}

	endTurn() {
		api.takeTurn(this.turn);
		this.waiting = true;
	}
}

var state: State = new State();