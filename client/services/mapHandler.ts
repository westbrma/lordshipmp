const TILE_SIZE = 16;
const HALF_TILE = TILE_SIZE / 2;
const DEFAULT_SIZE = 64;
const MIN_SIZE = 32;

class MapHandler {
	elBoard: HTMLElement = null;
	elMain: HTMLElement = null;
	elOverlay: HTMLElement = null;
	dragPos: Position2 = null;
	dragScrollPos: Position2 = null;
	dragging = false;
	scrollPos: Position2 = new Position2(0, 0);
	tileSize = DEFAULT_SIZE;
	_mousePos: Position2 = new Position2(0, 0);
	zoomSize: number = DEFAULT_SIZE;

	setTeamStyles() {
		var styles = document.getElementById('teamColors');
		if (styles) {
			styles.parentNode.removeChild(styles); // remove these styles
		}

		var styleTag = '<style id="teamColors">\n';
		state.armies.forEach(t => {
			styleTag += `.tile.${t.colorClass}{background-color:${t.color}}\n`
			styleTag += `.tile.${t.colorClassBG}{background-color:${t.darkColor}}\n`
		})
		styleTag += '</style>'
		$('html > head').append($(styleTag));
	}

	onMouseWheel(event: WheelEvent) {
		if (!state.game.active) return;

		this._mousePos = new Position2(event.clientX, event.clientY);

		var delta = -event.deltaY;
		delta = delta > 0 ? TILE_SIZE : -TILE_SIZE;

		var newSize = this.zoomSize + delta;
		this.setSize(newSize);
		event.preventDefault();
	}

	initWindowEvents() {
		this.elMain = document.getElementById('app');
		this.elMain.onmousemove = this.onMouseMove.bind(this);
		this.elMain.onmousedown = this.onMouseDown.bind(this);
		this.elMain.onmouseup = this.onMouseUp.bind(this);
		this.elMain.onwheel = this.onMouseWheel.bind(this);

		this.elBoard = document.getElementById('board');
		this.elBoard.onclick = this.onClick.bind(this);

		this.elOverlay = document.getElementById('dragOverlay');
		this.elOverlay.onmousemove = this.onMouseMove.bind(this);
		this.elOverlay.onmouseup = this.onMouseUp.bind(this);

		document.onkeydown = (ev: KeyboardEvent) => {
			if (ev.keyCode == 187) {
				this.setSize(this.zoomSize + 20);
			} else if (ev.keyCode == 189) {
				this.setSize(this.zoomSize - 20);
			}
		}
	};

	onClick(event: MouseEvent) {
		event.preventDefault();
		let tile = state.overTile;

		/*if (state.debugging) {
			if (tile.occupant instanceof Army) {
				state.setDebugTeam(tile.occupant.team);
			} else if (state.turnTeam == state.teams[0]) {
				state.teams[0].endTurn();
			}
			return;
		}*/

		if (!state.isPlayerTurn || this.dragging) {
			return;
		}

		if (state.expanding) {
			if (state.city.canExpand(tile)) {
				state.city.expand(tile);
				state.setOverTile(null);
				if(state.city.expPts == 0){
					state.expanding = false;
					state.selected = null;
				}
			} else if (tile.occupant == state.playerArmy) {
				state.selected = null;
			}
			return;
		} 
		
		if (tile.occupant != null && tile.isVisible) {
			state.selected = tile.occupant;
			
			if(state.city){
				CityDlg.open(state.city);
			}
			if (!state.city && (tile.occupant == state.playerArmy || tile.occupant.type != ObjType.Army)) {
				partyDlg.open();
			}
		} else if (state.playerArmy.canMove(tile)) {
			state.turn.moveArmy(tile);
			playSound(Sound.Walk);
			//board.addAnimation(tile, 1000);
			state.endTurn();
		} else {
			info.setMessage('You can only move 1 day per turn');
		}
	}

	onMouseDown(event: MouseEvent) {
		if (!this.dragging) {
			var pos: Position2 = new Position2(event.clientX, event.clientY);
			this.dragPos = pos;
		}
	}

	onMouseUp(event: MouseEvent) {
		if (this.dragging) {
			this.dragging = false;
			state.dragging = false;
			$(this.elOverlay).hide();
			event.preventDefault();
			event.stopPropagation();
		}
	}

	onMouseMove(event: MouseEvent) {
		var pos: Position2 = new Position2(event.clientX, event.clientY);

		if (event.buttons == 1) {
			if (event.movementX || event.movementY) {
				if (!this.dragging) {
					var dist = pos.dist(this.dragPos);
					if (dist > 3) {
						this.dragging = true;
						eventBus.fireEvent(Events.startDragging);
						state.setOverTile(null);
						state.dragging = true;
						this.dragScrollPos = this.getScrollPosition();
						$(this.elOverlay).show();
					}
				}
				else if (this.dragging) {
					var diff = pos.diff(this.dragPos);
					diff.x += this.dragScrollPos.x;
					diff.y += this.dragScrollPos.y;
					this.setScrollPos(diff);
					//clearSelection();
				}
			}
		} else if (state.gameActive) {
			let x = pos.x - this.scrollPos.x;
			let y = pos.y - this.scrollPos.y;
			let tile = board.tile(Math.floor(x / this.tileSize), Math.floor(y / this.tileSize));
			state.setOverTile(tile);
		}
	}

	getScrollPosition(): Position2 {
		return new Position2(this.scrollPos.x, this.scrollPos.y);
	}

	setScrollPos(pos: Position2) {
		this.scrollPos.x = pos.x;
		this.scrollPos.y = pos.y;
		this.elBoard.style.top = pos.y + "px";
		this.elBoard.style.left = pos.x + "px";
	}

	setSize(size: number) {
		if (size < MIN_SIZE) return;
		this.zoomSize = size;
		var origWorldPos = new Position2(this._mousePos.x, this._mousePos.y);
		origWorldPos.x -= this.scrollPos.x;
		origWorldPos.y -= this.scrollPos.y;

		var percentChange = size / this.tileSize;
		this.tileSize = size;

		if (board) {
			this.elBoard.style.width = board.width * this.tileSize + 'px';
			this.elBoard.style.height = board.height * this.tileSize + 'px';
		}

		var newWorldPos = new Position2(origWorldPos.x * percentChange, origWorldPos.y * percentChange);
		var newScrollPos = new Position2(-newWorldPos.x + this._mousePos.x, -newWorldPos.y + this._mousePos.y);
		this.setScrollPos(newScrollPos);
		eventBus.fireEvent(Events.zoomChanged);
		//eventBus.fireEvent(Events.zoomChanged);
	}

	centerWorldPos(pos: Position2) {
		var center: Position2 = new Position2(window.innerWidth / 2, window.innerHeight / 2);
		var diff = center.diff(pos);
		this.setScrollPos(diff);
	}
}

let handler = new MapHandler();