enum Events {
	overTileChanged = 'over-tile-changed',
	startDragging = 'start-dragging',
	zoomChanged = 'zoom-changed'
}


class EventBus extends Vue {

	fireEvent(arg1?, arg2?, arg3?, arg4?, arg5?) {
		this.$emit.apply(this, arguments);
	}

}

let eventBus = new EventBus();