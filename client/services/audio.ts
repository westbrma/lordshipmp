enum Sound{
	Walk='./sounds/Step.wav'
}

var audioEnabled = true;
var _sounds:{[key:string]:HTMLAudioElement} = {};

function playSound(sound:Sound){
	if(!audioEnabled) return;
	
	var audio = _sounds[sound];
	if(!audio){
		audio = new Audio(sound);
		_sounds[sound] = audio;
	}
	
	var p = audio.play();
	if(p){
		p.catch(()=>{});
	}
}