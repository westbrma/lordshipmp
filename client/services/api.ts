var socket: WebSocket = null;
var requests: { [key: number]: WsRequest } = {};

function makeMsgId(): string {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	for (var i = 0; i < 5; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

class WsRequest {
	defer: Defer<any> = new Defer();
}

enum SocketState {
	CONNECTING = 0,
	OPEN = 1,
	CLOSING = 2,
	CLOSED = 3
}

class API {
	init():Promise<any> {
		return new Promise((resolve, reject) => {
			socket = new WebSocket(config.wssUrl);

			socket.onopen = function () {
				state.socketAlive = true;
				resolve();
			};
	
			socket.onclose = (event:CloseEvent)=> {
				console.log("Socket Closed: " + event.code + " " + event.reason);
				state.socketAlive = false;
				reject();
				this.onGameDestroyed();
			};
	
			socket.onmessage = (evt) => {
				this.recieveMessage(evt);
			}
		});
	}

	recieveMessage(evt: MessageEvent) {
		var msg: IMessage = JSON.parse(evt.data);
		if (msg.error) {
			ErrorDlg.open(msg.error);
		}

		switch (msg.type) {
			case MessageType.PostMessage:
				this.recievePost(msg as IPostMsg);
				break;
			case MessageType.UpdateGame:
				this.onUpdateGame(msg as IUpdateGame);
				break;
			case MessageType.GameDestroyed:
				this.onGameDestroyed(msg);
				break;
			case MessageType.GameInfo:
				this.onGameInfo(msg as IGameInfo);
				break;
			case MessageType.GameList:
				this.onGameList(msg as IGameListMsg);
				break;
		}

		if (msg.id) {
			var req = requests[msg.id];
			if (req) {
				req.defer.return(msg.error == undefined, msg);
				requests[msg.id] = undefined;
			}
		}
	}

	onGameDestroyed(msg?: IMessage) {
		if (state.game) {
			state.reset();
			app.fireEvent(AppEvent.GameDestroyed)
			MainLobbyDlg.open();
		}
	}

	recievePost(msg: IPostMsg) {
		state.messages.push(msg.messsage);
	}

	onGameList(msg: IGameListMsg) {
		state.games.splice(0, state.games.length);

		msg.games.forEach(g => {
			var game = new Game();
			game.populate(g);
			state.games.push(game);
		});
	}

	createGame(game: Game): Promise<Game> {
		var req: ICreateGameMsg = {
			type: MessageType.CreateGame,
			game: game
		}

		return this._sendRequest(req).then((r: ICreateGameMsg) => {
			var game = new Game();
			game.populate(r.game);
			return game;
		});
	}

	joinGame(game: Game, user: Player): Promise<any> {
		var msg: IJoinGameMsg = {
			type: MessageType.JoinGame,
			gameId: game.id,
			player: user
		}

		return this._sendRequest(msg);
	}

	updatePlayer(player:IPlayer) {
		var msg : IUpdatePlayer = {
			type: MessageType.UpdatePlayer,
			player : player
		}

		this._send(msg);
	}

	startGame(game:IStartGame){
		game.type = MessageType.StartGame;
		this._send(game);
	}

	leaveGame(game: Game) {
		var msg: ILeaveGame = {
			type: MessageType.LeaveGame,
			gameId: game.id
		}

		state.reset();
		this._send(msg);
		MainLobbyDlg.open();
	}

	postMessage(message: string) {
		if(!message) return;

		var msg: IPostMsg = {
			type: MessageType.PostMessage,
			messsage: `${state.player.name}: ${message}`
		}

		this._send(msg);
	}

	_send(msg: IMessage) {
		socket.send(JSON.stringify(msg));
	}

	_sendRequest(msg: IMessage): Promise<any> {
		msg.id = makeMsgId();
		var req = new WsRequest();
		requests[msg.id] = req;
		this._send(msg);
		return req.defer.promise;
	}

	takeTurn(turn: ITakeTurn) {
		this._send(turn);
	}

	close() {
		socket.close();
	}

	onGameInfo(msg: IGameInfo) {
		if (state.game.id == msg.game.id) {
			msg.game.players.forEach(p => {
				var found = state.game.players.find(player => player.id == p.id);
				if (!found) {
					state.game.players.push(p);
				}else{
					found.color = p.color;
				}
			});

			for (let i = state.game.players.length - 1; i >= 0; i--) {
				let p = state.game.players[i];
				if (msg.game.players.find(player => player.id == p.id) == null) {
					state.game.players.splice(i, 1);
				}
			}
		}
	}

	onUpdateGame(msg: IUpdateGame) {
		if (msg.board) {
			state.initBoard(msg.board);
		}

		state.pendingTargets.forEach(t => t.pendingExpedition = false);

		msg.mapObjects.forEach(o => {
			var obj = state.mapObjects[o.id];
			if (obj) {
				obj.update(o);
			} else {
				if (o.type == ObjType.Army) {
					obj = new Army(o as IArmy);
				}
				else if (o.type == ObjType.City) {
					obj = new City(o as ICity);
				}
				else {
					obj = new MapObject(o);
				}
			}
		});

		if (msg.deletedIds) {
			msg.deletedIds.forEach(id => {
				var obj = state.mapObjects[id];
				if (obj) {
					obj.destroy();
				}
			});
		}

		if (msg.tileUpdates) {
			msg.tileUpdates.forEach(t => {
				var tile = board.tile(t.x, t.y);
				tile.city = state.mapObjects[t.cityId] as City;
			});
		}

		if (msg.myInfo.visTiles) {
			var visTiles = msg.myInfo.visTiles;
			var fogTiles = new Map<Tile, boolean>();
			state.visTiles.forEach(t => {
				fogTiles.set(t, true);
			});

			state.visTiles = [];
			for (let i = 0; i < visTiles.x.length; i++) {
				var tile = board.tile(visTiles.x[i], visTiles.y[i]);
				tile.setVisibility(TileVis.Visible);
				state.visTiles.push(tile);
				fogTiles.delete(tile);
			}

			fogTiles.forEach((v, t) => {
				t.setVisibility(TileVis.Fog);
			});
		}

		if (msg.myInfo.informs && info) {
			msg.myInfo.informs.forEach(m => {
				info.setMessage(m);
			});
		}

		state.playerArmy.gold = msg.myInfo.gold;
		state.playerArmy.food = msg.myInfo.food;

		state.playerArmy.cards = [];
		msg.myInfo.cards.forEach(c => {
			state.playerArmy.addCard(new Card(c));
		});

		if (msg.start) {
			state.game.active = true;
			state.start();
		}

		state.turn.reset();
		state.waiting = false;
	}
}

var api = new API();