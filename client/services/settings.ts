enum SettingType {
	playerName = 'player_name'
}

class Settings {
	get<T>(type: SettingType, defaultValue?: T): T {
		let result: any = localStorage.getItem(type);
		if (result && result.toLowerCase === undefined) {
			result = JSON.parse(result);
		}

		if (result == undefined && defaultValue != undefined) {
			result = defaultValue;
		}

		return <T>result;
	}

	set(type: SettingType, obj: any) {
		if (obj.toLowerCase === undefined) {
			obj = JSON.stringify(obj);
		}
		localStorage.setItem(type, obj);
	}
}

var settings = new Settings();