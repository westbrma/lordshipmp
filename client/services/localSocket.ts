class LocalSocket implements WebSocket {
	binaryType: BinaryType;
	readonly bufferedAmount: number;
	readonly extensions: string;
	onclose: ((this: WebSocket, ev: CloseEvent) => any) | null;
	onerror: ((this: WebSocket, ev: Event) => any) | null;
	onmessage: ((this: WebSocket, ev: MessageEvent) => any) | null;
	onopen: ((this: WebSocket, ev: Event) => any) | null;
	readonly protocol: string;
	readonly readyState: number;
	readonly url: string;
	
	close(code?: number, reason?: string): void{

	}

	send(data: string | ArrayBufferLike | Blob | ArrayBufferView): void {
		var msg: IMessage = JSON.parse(<string>data);
		/*switch (msg.type) {
			case MessageType.JoinGame:
				this.onJoinGame(client, msg as IJoinGameMsg);
				break;
			case MessageType.LeaveGame:
				this.onLeaveGame(client, msg as ILeaveGame);
				break;
			case MessageType.PostMessage:
				this.broadcast(msg, client.game);
				break;
			case MessageType.TakeTurn:
				client.game.takeTurn(client.player, msg as ITakeTurn);
				break;
			case MessageType.StartGame:
				this.onStartGame(client, msg);
				break;
			case MessageType.CreateGame:
				this.onCreateGame(client, msg as ICreateGameMsg);
				break;
			case MessageType.UpdatePlayer:
				this.onUpdatePlayer(client, msg as IUpdatePlayer)
				break;
		}*/
	}

	readonly CLOSED: number;
	readonly CLOSING: number;
	readonly CONNECTING: number;
	readonly OPEN: number;

	addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void {}
	removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void {}

    dispatchEvent(evt: Event): boolean {
		 return false;
	 }
}