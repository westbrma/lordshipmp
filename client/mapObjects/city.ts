class City extends MapObject {
	expPts:number;
	activeProd:IProdDef;
	prodAmount:number;
	production:number;
	gold:number;
	food:number;
	population:number;

	constructor(city:ICity) {
		super(city);
		this.name = city.name;
		this.setIcon(this.icon);
		this.team.cities.push(this);
		makeReactive(this);
	}

	update(obj:ICity){
		super.update(obj);
		if(obj.expPts != undefined){
			this.expPts = obj.expPts;
			this.production = obj.production;
			this.food = obj.food;
			this.gold = obj.gold;
			this.population = obj.population;
			this.activeProd = prodMap[obj.prod];
			this.prodAmount = obj.prodAmount;
		}
	}

	static canBuild(tile: Tile): boolean {
		if (tile.city == null && [TileType.Grass, TileType.Sand, TileType.Dirt, TileType.Trees, TileType.Mountain].indexOf(tile.type) >= 0) {
			return true;
		}

		return false;
	}

	canExpand(tile: Tile): boolean {
		if (tile.city != null) {
			if (this.expPts < 2
				|| tile.city.team == this.team
				|| tile.occupant == tile.city) {
				return false;
			}
		}
		else if (this.expPts < 1) {
			return false
		}

		var adj = false;
		tile.adjacent.forEach(t => {
			if (t.city == this) {
				adj = true;
			}
		});
		return adj;
	}

	changeProduction(type:IProdDef){
		let u = state.turn.getCityUpdate(this);
		u.prodType = type.name;
		this.activeProd = prodMap[type.name];
	}

	expand(tile: Tile) {
		let u = state.turn.getCityUpdate(this);
		u.expansions.push({ x: tile.x, y: tile.y });
		tile.city = this;
		tile.render();
		tile.adjacent.forEach(t => {
			t.setVisibility(TileVis.Visible);
		});

		state.setOverTile(null);
		this.expPts--;
	}
}