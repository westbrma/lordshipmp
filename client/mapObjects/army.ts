class Army extends MapObject {
	action: any = null;
	cards: Card[] = [];
	nonActive: number = 0;
	units: number = 0;
	color:string;
	darkColor:string;
	colorClass: string = makeid();
	colorClassBG: string = makeid();
	player:Player = null;
	state = state;

	cities: City[] = [];
	score: number = 0;
	gold: number = 0;
	food: number = 0;

	constructor(army:IArmy) {
		super(army);
		this.team = this;
		this.player = state.game.players.find(p => p.id == this.id);
		if(this.player){
			this.color = this.player.color;
			this.name = this.player.name;
		}else{
			this.color = state.getNextColor();
			this.name = 'AI_Player'+state.armies.length;
		}

		this.darkColor = darkenRgb(this.color, .25);
		this.updateIcon();
		
		makeReactive(this);

		state.armies.push(this);
		if(this.id == state.player.id){
			state.playerArmy = this;
		}
	}

	get scoreBG():string {
		let key = getColorImageKey("images/scoreBG.png", this.color);
		return `url(${genImageMap[key].src})`;
	}

	get scoreWidth():string {
		let percent = (this.score/this.state.winScore)*100;
		if(percent < 10){
			percent = 10;
		}
		return percent + '%';
	}

	update(obj:IArmy){
		super.update(obj);
		this.score = obj.score;
		this.updateIcon();
	}

	updateIcon(){
		if(!this.color) return;
		setObjectIcon(this);
	}

	buildCity(name?: string) {
		state.turn.cityUpdates.push({
			newCity: true,
			cityName: name
		});
		state.endTurn();
	}

	canMove(tile:Tile) {
		return this.tile.getDistanceFrom(tile) <= 1;
	}

	hasRequiredItems(obj:MapObject): boolean {
		var req = obj.objectives.find(o => o.type == TaskType.Requirement);
		if (req) {
			var item = this.cards.find(c => c.name == req.itemName);
			return item != null;
		}
		return true;
	}

	getCollectable(tile: Tile) {
		if (tile.occupant != null && tile.occupant.collectable) {
			tile.occupant.destroy();
		}
	}

	addCard(card: Card) {
		card.army = this;
		this.cards.push(card);
		this.cards.sort((a, b) => {
			if (a.def.isUnit != b.def.isUnit) {
				return a.def.isUnit ? -1 : 1;
			}

			var x = a.def.name;
			var y = b.def.name;
			return x < y ? -1 : x > y ? 1 : 0;
		});
		this.calcHP();
	}

	removeCard(unit: Card) {
		removeFromArray(unit, this.cards);
		this.calcHP();
	}

	calcHP() {
		let hp = 0;
		let nonActive = 0;
		let units = 0;

		this.cards.forEach(u => {
			if (u.def.isUnit) {
				units++;

				if (u.onMission == null) {
					nonActive++;
					hp += u.hp;
				}
			}
		});

		this.units = units;
		this.nonActive = nonActive;
		this.hp = hp;
	}

	get canAffordCity():boolean {
		return this.gold >= 100;
	}

	async wait(milliseconds: number) {
		return new Promise((resolve) => {
			setTimeout(() => resolve(), milliseconds);
		});
	}
}