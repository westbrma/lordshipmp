interface IObjectiveVM extends IObjective {
	icon: string;
	name: string | number;
}

const NPC_COLOR = '#ff0000'

class MapObject {
	id: number;
	tile: Tile = null;
	team: Army = null;
	Active: boolean;
	icon: string = '';
	collectable: boolean = false;
	type: ObjType;
	selected: boolean = false;
	hp: number = 0;
	name: string = null;
	reward: IReward = null;
	objectives: IObjectiveVM[] = []
	rewardIcon: string = null;
	skills: SkillLevelMap = {};
	pendingExpedition: boolean = false;
	eta?: number;
	travelRemaing?: number;

	constructor(obj: IMapObject) {
		this.id = obj.id;
		this.type = obj.type;
		if(obj.type == ObjType.Expedition){
			this.name = "Expedition";
		}

		state.mapObjects[this.id] = this;
		if (obj.teamId) {
			this.team = state.armies.find(t => t.id == obj.teamId);
		}

		if (obj.reward) {
			this.reward = obj.reward;
			this.rewardIcon = rewardIcons[this.reward.type];
		}

		obj.objectives.forEach(o => {
			this.objectives.push({
				type: o.type,
				skill: o.skill,
				amount: o.amount,
				itemName: o.itemName,
				icon: o.skill ? skillMap[o.skill].icon : cardMap[o.itemName].icon,
				name: o.amount < 1 ? Math.round(o.amount * 100) + '%' : o.amount
			});
		});

		this.update(obj);
		setObjectIcon(this);
	}

	update(obj: IMapObject) {
		if (obj.hp) {
			this.hp = obj.hp;
		}

		if (obj.tile) {
			this.setTile(obj.tile);
		}

		if (obj.eta !== undefined) {
			this.eta = obj.eta;
		}

		if (obj.travelRemaing !== undefined) {
			this.travelRemaing = obj.travelRemaing;
		}

		if (obj.skills) {
			this.skills = obj.skills;
		}
	}

	setTile(coord: ICoord) {
		var newTile = board.tile(coord.x, coord.y);
		if (this.tile != newTile) {
			if (this.tile && this.tile.occupant == this) {
				this.tile.setOccupant(null);
			}

			newTile.setOccupant(this);
			this.tile = newTile;
		}
	}

	get isBattleTarget() {
		return this.hp > 0;
	}

	get hpColor(): string {
		return this.team ? this.team.color : NPC_COLOR;
	}

	get hpIcon(): HTMLImageElement {
		if (this.hp > 0) {
			let key = getColorImageURL(`number:${this.hp}.png`, this.hpColor);
			return genImageMap[key];
		}
	}

	getDistanceFrom(mapObject: MapObject): number {
		return this.tile.getDistanceFrom(mapObject.tile);
	}

	destroy() {
		if (this.tile && this.tile.occupant == this) {
			this.tile.occupant = null;
		}
		state.mapObjects[this.id] = undefined;
	}

	setIcon(src: string) {
		this.icon = src;
		if (this.team) {
			this.icon = getColorImageURL(src, this.team.color);
		}
	}
}