@VueComponent({
	template:
		`<div id="infoBar" v-show="lines.length">
			<div v-for="message in lines">{{message}}</div>
		</div>`
})
class InfoBar {
	lines:string[] = [];
	timers:number[] = [];

	created(){
		info = this;
		setInterval(()=>{
			for(var i=this.lines.length-1; i>=0 ; i--){
				var t = this.timers[i];
				t--;
				if(t <= 0){
					this.timers.splice(i, 1);
					this.lines.splice(i, 1);
				}else{
					this.timers[i] = t;
				}
			}
		}, 1000);
	}

	setMessage(message: string) {
		removeFromArray(message, this.lines);
		this.lines.push(message);
		this.timers.push(4);
	}
}

var info: InfoBar = null
Vue.component('info', InfoBar);