@VueComponent({
	template:
	`<div id="stats">
		<span style="color:orange">Food: {{army.food}}</span>
		<span style="color:#42ff86">
			HP: {{army.hp}}
		</span>
		<span style="color:gold">Gold: {{team.gold}}</span>
	</div>`
})
class Stats{
	army:Army = state.playerArmy;
}

Vue.component('stats', Stats);