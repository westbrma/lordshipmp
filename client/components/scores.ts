@VueComponent({
	template:
		`<div id="scores">
			<div class="max">
				<div class="tick" style="left:24%"></div>
				<div class="tick" style="left:49%"></div>
				<div class="tick" style="left:74%"></div>
			</div>
			<div v-for="team in state.armies" >
				<div :style="{background:team.color, width:team.scoreWidth}" style="display:inline-block" class="teamScore">
					<div class="top">
						<span class="hover">{{team.name}}: {{team.score}}</span>
					</div>
				</div>
			</div>
	</div>`
})
class Scores {
	state: State = state;
}

Vue.component('scores', Scores);