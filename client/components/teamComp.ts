@VueComponent({
	template:
	`<div id="teamObjects">
		<div class="teamObj" @click="gotoPlayer()">
			{{state.playerArmy.name}}
		</div>
		<div class="teamObj" v-for="city in state.playerArmy.cities" @click="gotoCity(city)">
			<span>{{city.name}}</span>
			<span v-show="city.expPts > 0" style="color:aqua"> Expand Pts: {{city.expPts}}</span>
			<span v-show="city.mission" style="color:red">Under Attack!</span>
		</div>
		<div>
			<div style="color:gold">Gold: {{state.playerArmy.gold}}</div>
			<div v-if="state.playerArmy.food > 0" style="color:#42ff86">Food: {{state.playerArmy.food}}</div>
			<span v-else style="color:red">Out of Food!</span>
		</div>
	</div>`
})
class TeamComp{
	state:State = null;

	created(){
		this.state = state;
	}

	gotoCity(city:City){
		state.selected = city;
		CityDlg.open(state.city);
		board.scrollToTile(city.tile);
	}

	gotoPlayer(){
		state.selected = state.playerArmy;
		board.scrollToTile(state.playerArmy.tile);
	}
}

Vue.component('team-comp', TeamComp);