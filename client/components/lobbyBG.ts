class HomeTile {
	x: number;
	y: number;
	image: HTMLImageElement;
	alpha: number = 0;

	constructor(x:number, y:number, image:HTMLImageElement) {
		this.x = x;
		this.y = y;
		this.image = image;
	}

	getDistanceFrom(fromTile: HomeTile): number {
		let xDist = Math.abs(this.x - fromTile.x);
		let yDist = Math.abs(this.y - fromTile.y);
		return xDist + yDist;
	}
}

const HomeTileSize = 64;

@VueComponent({
	template: '<canvas id="lobbyBG"></canvas>'
})
class LobbyBG extends Vue {
	canvas: HTMLCanvasElement;
	width: number;
	height: number;
	tiles: HomeTile[][] = [];
	bindEvent:any;
	resizeHandler:any;

	mounted() {
		this.bindEvent = this.onMouseMove.bind(this);
		this.resizeHandler = this.onResize.bind(this);
		document.addEventListener('mousemove', this.bindEvent);
		//window.addEventListener('resize', this.resizeHandler);

		this.canvas = <HTMLCanvasElement>this.$el;
		this.generate();
	}

	destroyed() {
		document.removeEventListener('mousemove', this.bindEvent);
		window.removeEventListener('resize', this.resizeHandler);
	}

	onResize(){
		this.canvas.width = this.$el.clientWidth;
		this.canvas.height = this.$el.clientHeight;
		this.renderMap();
	}

	generate(){
		this.tiles = [];
		this.canvas.width = this.$el.clientWidth;
		this.canvas.height = this.$el.clientHeight;

		let w = this.width = Math.ceil(this.canvas.width / HomeTileSize);
		let h = this.height = Math.ceil(this.canvas.height / HomeTileSize);

		var TileTypeArr: TileType[] = [TileType.Grass, TileType.Mountain, TileType.Trees, TileType.Dirt, TileType.Sand, TileType.Water];

		for (var y = 0; y < h; y++) {
			var row: HomeTile[] = [];
			this.tiles.push(row);

			for (var x = 0; x < w; x++) {
				var type:TileType = getRandomItem(TileTypeArr);
				let def = tileDefs._defs[type];
				var tile = new HomeTile(x, y, getRandomItem(def.images));
				row.push(tile);
			}
		}
	}

	tile(x: number, y: number):HomeTile {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
			return null;
		}
		return this.tiles[y][x];
	}

	onMouseMove(event: MouseEvent) {
		var pos: Position2 = new Position2(event.clientX, event.clientY);
		let source = this.tile(Math.floor(pos.x / HomeTileSize), Math.floor(pos.y / HomeTileSize));
		if (source == null) return;
		let radius = 3;
		let speed = .05;

		for (var x = -radius; x <= radius; x++) {
			for (var y = -radius; y <= radius; y++) {
				var tile = this.tile(x + source.x, y + source.y);
				if (tile != null && source.getDistanceFrom(tile) <= radius) {
					if (tile.alpha == undefined) {
						tile.alpha = speed;
					} else {
						tile.alpha += speed;
						if (tile.alpha > 1) tile.alpha = 1;
					}
				}
			}
		}

		this.renderMap();
	}

	renderMap() {
		let ctx = this.canvas.getContext('2d');
		ctx.globalAlpha = 1;
		ctx.fillStyle = 'black';
		ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
		ctx.imageSmoothingEnabled = false;

		for (let y = 0; y < this.tiles.length; y++) {
			let row = this.tiles[y];
			for (let x = 0; x < row.length; x++) {
				let t = row[x];
				let xPos = t.x * HomeTileSize;
				let yPos = t.y * HomeTileSize
				ctx.globalAlpha = t.alpha ? t.alpha : 0;
				ctx.drawImage(t.image, xPos, yPos, HomeTileSize, HomeTileSize);
			}
		}
	}
}

Vue.component('lobby-bg', LobbyBG);