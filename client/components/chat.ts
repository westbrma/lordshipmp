@VueComponent({
	template:
		`<div id="chat">
			<div v-for="msg in state.messages">
				{{msg}}
			</div>
			<input :class="{active:show}" v-model="message" @keydown.enter.stop="sendMessage()" @blur="show=false" />
		</div>`
})
class Chat extends Vue{
	state: State = state;
	message = '';
	show:boolean = false;

	created(){
		state.messages.length = 0;

		this.$watch('state.messages', ()=>{
			/*if(state.messages.length > 5){
				let removeCount = state.messages.length - 5;
				state.messages.splice(0, removeCount);
			}*/

			setTimeout(()=>{
				if(state.messages.length > 0){
					state.messages.splice(0, 1);
				}
			}, 15000);
		});

		document.onkeydown = (e: KeyboardEvent) => {
			if (e.keyCode == 13 && !this.show) {
				this.show = true;
				Vue.nextTick(() => {
					$(this.$el).find('input').focus();
				});
			}
		}
	}

	sendMessage() {
		if(this.message){
			api.postMessage(this.message);
			this.message = '';
		}
	}
}

Vue.component('chat', Chat);