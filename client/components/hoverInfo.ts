@VueComponent({
	template:
		`<div id="hoverInfo" v-show="info.length > 0">
		<div v-for="item in info" class="item">{{item}}</div>
	</div>`
})
class HoverInfo extends Vue {
	info: string[] = [];
	state = state;

	created() {
		this.state = state;
		eventBus.$on(Events.zoomChanged, this.updatePos);
		eventBus.$on(Events.overTileChanged, this.refresh);
	}

	updatePos() {
		if (this.info.length == 0) return;

		Vue.nextTick(() => {
			var offset = state.overTile.getOffset();
			offset.x += handler.tileSize + 10;
			offset.y += 10;

			var el = this.$el;
			var diff = document.body.clientHeight - (offset.y + el.clientHeight);
			if (diff < 0) {
				offset.y += diff;
			}
			diff = document.body.clientWidth - (offset.x + el.clientWidth);
			if (diff < 0) {
				offset.x += diff;
			}
			el.style.top = offset.y + 'px';
			el.style.left = offset.x + 'px';
		});
	}

	refresh() {
		this.info = [];
		if(!state.overTile) return;

		var obj: MapObject = null;
		if (state.overTile && state.overTile.isVisible && state.overTile.occupant) {
			obj = state.overTile.occupant;
		} else if (state.selected && state.selected.tile.isVisible) {
			obj = state.selected;
		}

		let tt = state.playerArmy.tile.getDistanceFrom(state.overTile);

		if (state.expanding) {
			let canExpand = state.city && state.city.canExpand(state.overTile);
			if (canExpand) {
				this.info.push('Expand');
			}
		}
		else if(obj == state.playerArmy){
			this.info.push('Build');
		}
		else if (obj) {
			this.info.push('travel: ' + tt);

			if (obj.hp > 0) {
				this.info.push('HP: ' + obj.hp);
			}

			for (let prop in obj.skills) {
				let def = skillMap[prop];
				let value: any = obj.skills[prop];
				if (def.type == SkillType.Combat) {
					value = `1-${1 + value}`;
				}
				this.info.push(`${def.type}: ${value}`);
			}

			obj.objectives.forEach(o => {
				let prompt: any = o.type;
				if (o.skill) {
					prompt = o.skill;
				}

				let value: any = o.amount;
				if (o.type == TaskType.Chance) {
					value = Math.round(o.amount * 100) + '%';
				} else if (o.type == TaskType.Requirement) {
					value = o.itemName;
				}

				this.info.push(`${prompt}: ${value}`);
			})

			if (obj.reward) {
				let amount = ''
				if (obj.reward.amount > 0) {
					amount = ' ' + obj.reward.amount + ' ';
				}
				this.info.push(`Reward: ${amount}${obj.reward.type}`);
			}
		} else if(tt > 0) {
			this.info.push(tt.toString());
		}

		this.updatePos();
	}
}

Vue.component('hover-info', HoverInfo);