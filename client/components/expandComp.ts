@VueComponent({
	template:
		`<div id="expand" v-if="state.expanding">
			<div>
				<button @click="finish()">Done</button>
			</div>
			<br/>
			<div>Click adjacent tiles to expand city</div>
		</div>`
})
class ExpandComp extends Vue{
	state: State = state;

	finish(){
		state.expanding = false;
		state.selected = null;
	}
}

Vue.component('expand', ExpandComp);