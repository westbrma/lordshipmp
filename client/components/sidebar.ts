@VueComponent({
	template:
	`<div id="sidebar">
		<div id="buttons">
			<button @click="openHelp()">Help</button>
			<button @click="openMenu()">Menu</button>
		</div>
		<div id="tileInfo">
			<div v-for="item in info">{{item}}</div>
		</div>
	</div>`
})
class Sidebar{
	info:string[] = [];

	created(){
		sidebar = this;
		eventBus.$on(Events.overTileChanged, this.refresh);
		this.refresh();
	}

	refresh(){
		if(state.overTile == null || state.overTile.visibility == TileVis.Hidden){
			return;
		}

		var def = tileDefs.getDef(state.overTile.type);
		this.info = [];
		this.info.push(def.Name);
		this.info.push('Food: ' + def.Food);
		this.info.push('Gold: ' + def.Gold);
		this.info.push('Production: ' + def.Production);
		this.info.push('Population: ' + def.Population);
	}

	openHelp(){
		HowToPlayDlg.open();
	}

	openMenu(){
		menu.open();
	}
}

var sidebar:Sidebar = null;
Vue.component('sidebar', Sidebar);