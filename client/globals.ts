var board:Board = null;

interface IConfig {
	version: number;
	isProd: boolean;
	wssUrl: string;
}

declare module '*.html' {
	const value: string;
	export default value
}

declare var config: IConfig;
declare var gtag;

declare var VueComponent: any;
declare var VueClassComponent;
var VueComponent: any = VueClassComponent.default;