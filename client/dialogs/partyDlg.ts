@VueComponent({
	template: templates.fetch('partyDlg')
})
class PartyDlg extends Vue {
	cards: Card[] = [];
	state: State = null;
	skillMap = skillMap;
	unitCount:number = 0;
	expedition:Expedition = null;
	target: MapObject = null;

	open() {
		this.expedition = new Expedition(state.playerArmy, state.selected);
		this.cards = state.playerArmy.cards;
		this.unitCount = state.playerArmy.units;
		this.cards.forEach(u => u.selected = false);
		this.state = state;
		this.target = state.selected;

		if (this.$el == null) {
			dlg.open(this);
		}

		this.dialog.show();
	}

	get selfTarget():boolean{
		return this.target == this.state.playerArmy;
	}

	get canAttack(): boolean {
		return (this.target && this.target.isBattleTarget && this.target.team != state.playerArmy);
	}

	get canSend(): boolean {
		return this.target != null && !this.target.isBattleTarget && !this.selfTarget;
	}

	select(unit: Card) {
		unit.selected = !unit.selected;
		if(unit.selected){
			this.expedition.addCard(unit);
		}else{
			this.expedition.removeCard(unit);
		}
	}

	attack(){
		this.sendUnits();
	}

	sendUnits() {
		if (this.expedition.validate()) {
			this.expedition.send();
			this.close();
		}
	}

	close() {
		this.expedition = null;
		state.selected = null;
		this.dialog.hide();
	}

	buildCity(){
		if(!City.canBuild(state.playerArmy.tile)){
			info.setMessage('Cannot build city here');
			return;
		}

		this.close();
		NewCityDlg.open();
	}
}

var partyDlg = new PartyDlg();