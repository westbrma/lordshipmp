@VueComponent({
	template: templates.fetch('howToPlayDlg')
})
class HowToPlayDlg extends Vue {

	static open() {
		var vm = new HowToPlayDlg();
		dlg.open(vm);
		trackEvent(MetricAction.Help);
	}

	mounted() {
		this.$el.scrollTop = 0;
	}

	close() {
		dlg.close(this);
	}
}