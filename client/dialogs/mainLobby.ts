@VueComponent({
	template: templates.fetch('mainLobby')
})
class MainLobbyDlg extends Vue{
	games:Game[] = state.games;
	selectedGame:Game = null;
	state=state;

	static open(){
		let vm = new MainLobbyDlg();
		dlg.open(vm);
	}

	async createGame(){
		if(socket.readyState != SocketState.OPEN){
			await api.init();
		}

		if(!state.player.name){
			ErrorDlg.open('Enter Player Name.');
			return;
		}

		/*new NewGameDlg().open().then(game=>{
			api.createGame(game);
			api.connect(game, new Player());
		})*/
		api.createGame(new Game()).then(g => {
			this.selectedGame = g;
			this.connect();
		});
	}

	close(){
		dlg.close(this);
	}

	async connect() {
		if(socket.readyState != SocketState.OPEN){
			await api.init();
		}

		let player:IPlayer = state.player;
		let game = this.selectedGame;

		if(!player.name){
			ErrorDlg.open('Enter Player Name.');
			return;
		}

		if(!game){
			ErrorDlg.open('Select a game to join.');
			return;
		}

		if(game.active){
			ErrorDlg.open('Cannot join an active game');
			return;
		}

		let color:string = null;
		for(let i=0; i<colors.length; i++){
			color = colors[i];
			if(!game.players.find(p => p.color == color)){
				break;
			}
		}

		player.color = color;
		settings.set(SettingType.playerName, player.name);

		api.joinGame(this.selectedGame, player).then((r:IJoinGameMsg)=>{
			if(r.joined){
				state.game = this.selectedGame;
				player.id = r.player.id;
				GameLobbyDlg.open();
				this.close();
			}
		}).catch(err => {
			
		});
	}

	directions() {
		HowToPlayDlg.open();
	}

	viewCredits() {
		//CreditsDlg.open();
	}
}