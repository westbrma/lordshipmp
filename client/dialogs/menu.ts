@VueComponent({
	template: templates.fetch('menu')
})
class Menu extends Vue{
	isProd = config.isProd;
	close(){
		dlg.close(this);
	}

	revealMap(){
		state.debugging = true;
		board.exposeAll();
		this.close();
	}

	leaveGame(){
		api.leaveGame(state.game);
		this.close();
	}

	directions(){
		this.close();
		HowToPlayDlg.open();
	}
}

var menu = {
	open: function(){
		var vm = new Menu();
		dlg.open(vm);
	}
}