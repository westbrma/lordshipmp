@VueComponent({
	template: templates.fetch('cityDlg')
})
class CityDlg extends Vue {
	city: City = null;
	rename: boolean = false;
	prodList = prodList;

	static open(city: City) {
		var vm = new CityDlg();
		vm.city = city;
		dlg.open(vm);
	}

	mounted() {
		//var elName = this.$refs.elName;
		//elName.setSelectionRange(0, elName.value.length);
	}

	expand(){
		dlg.close(this);
		state.expanding = true;
	}

	setProd(item:IProdDef){
		this.city.changeProduction(item);
	}

	toggleRename() {
		this.rename = !this.rename;
		if (this.rename) {
			var elName: HTMLInputElement = this.$refs.elName;
			Vue.nextTick(() => {
				elName.focus();
				elName.setSelectionRange(0, elName.value.length);
				elName.focus();
			});
		}
	}

	close() {
		state.selected = null;
		dlg.close(this);
	}
}