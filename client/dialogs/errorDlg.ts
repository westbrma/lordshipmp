@VueComponent({
	template: templates.fetch('errorDlg')
})
class ErrorDlg extends Vue {
	errors = [];

	static open(errors:string[]|string) {
		let vm = new ErrorDlg();
		if(!Array.isArray(errors)){
			errors = [errors];
		}
		vm.errors = errors;
		dlg.open(vm);
	}

	close() {
		dlg.close(this);
	}
}