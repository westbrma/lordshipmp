interface IMapSize{
	width:number;
	height:number;
}

@VueComponent({
	template: templates.fetch('newGameDlg')
})
class NewGameDlg extends Vue{
	aiTeams:number = state.aiTeamCount;
	state = state;
	name:string = 'Arthur';
	winScore:number = state.winScore;
	colors:string[] = colors;
	selColor:string = colors[0];
	mapSizes: IMapSize[] = [{width:40, height:20}, {width:30, height:15}];
	mapSize:IMapSize = null;

	created(){
		this.mapSize = this.mapSizes[1];
		this.mapSizeChanged();
	}

	mounted(){
		var elName = this.$refs.elName;
		elName.setSelectionRange(0, elName.value.length)
	}

	mapSizeChanged(){
		var tileCount = this.mapSize.width * this.mapSize.height;
		this.aiTeams = Math.min(10, Math.ceil(10 * (tileCount/1000)));
	}

	start(){
		state.aiTeamCount = this.aiTeams;
		state.winScore = this.winScore;
		state.mapWidth = this.mapSize.width;
		state.mapHeight = this.mapSize.height;

		this.close();
	}

	directions(){
		HowToPlayDlg.open();
	}

	close(){
		dlg.close(this);
	}
}

var newGameDlg = {
	open: function(){
		var vm = new NewGameDlg();
		dlg.open(vm);
	}
}