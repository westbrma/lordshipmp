var genImageMap: { [key: string]: HTMLImageElement } = {};

var loadList = [
	'images/pendingExp.png',
	'images/army.png',
	'images/party.png',
	'images/armyBoat.png',
	'images/scoreBG.png',
	'images/tower.png',

	'images/animal.png',
	'images/battlefield.png',
	'images/bounty.png',
	'images/bounty2.png',
	'images/bountySea.png',
	'images/city.png',
	'images/discovery.png',
	'images/fish.png',
	'images/merchant.png',
	'images/wonder.png',
	'images/recruit.png',
	'images/mission.png',
	'images/waterMission.png',
	'images/chest.png',

	'images/numbers/0.png',
	'images/numbers/1.png',
	'images/numbers/2.png',
	'images/numbers/3.png',
	'images/numbers/4.png',
	'images/numbers/5.png',
	'images/numbers/6.png',
	'images/numbers/7.png',
	'images/numbers/8.png',
	'images/numbers/9.png',

	/*'images/tiles/farm.png',
	'images/tiles/gold.png',
	'images/tiles/grass.png',
	'images/tiles/dirt.png',
	'images/tiles/rock.png',
	'images/tiles/sand.png',
	'images/tiles/sheep.png',
	'images/tiles/tree.png',
	'images/tiles/village.png',*/

	'images/skills/charisma.png',
	'images/skills/combat.png',
	'images/skills/exploring.png',
	'images/skills/fishing.png',
	'images/skills/heart.png',
	'images/skills/hunting.png',
	'images/skills/key.png',
	'images/skills/sailing.png',
	'images/skills/trading.png',
	'images/skills/trap.png',

	'images/units/bard.png',
	'images/units/knight.png',
	'images/units/pirate.png',
	'images/units/thief.png',
	'images/units/woodsman.png',

	'images/cards/relic.png',
	'images/cards/princess.png',
	'images/cards/teleport.png',
	'images/cards/luckyDie.png',
	'images/cards/shield.png',

	'images/rewards/chest.png',
	'images/rewards/food.png',
	'images/rewards/gold.png',
	'images/rewards/score.png',
	'images/rewards/unit.png',

	'images/travelIcon.png'
];


var loadState = {
	total: loadList.length,
	complete: 0
}

function preload(): Promise<any> {
	var promises: Promise<any>[] = [];

	let tileImages = new Promise(resolve => {
		let total = 0;
		let completed = 0;
		for (let prop in tileDefs._defs) {
			let def = tileDefs._defs[prop];
			def.icons.forEach(i => {
				total++;
				let image = new Image();
				def.images.push(image);
				image.src = def.getIconUrl(i);
				image.onload = function () {
					completed++;
					if (total == completed) {
						resolve();
					}
				};
			});
		}
	});

	promises.push(tileImages);

	loadList.forEach(i => {
		var p = new Promise(resolve => {
			let img: HTMLImageElement = new Image();
			genImageMap[i] = img;
			img.src = i;
			img.onload = event => {
				loadState.complete++;
				resolve();
			}
		});

		promises.push(p);
	});

	return Promise.all(promises).then(r => {
		for (let i = 0; i <= 99; i++) {
			let img = buildNumber(i);
			let key = `number:${i}.png`
			genImageMap[key] = img;
		}
	});
}

@VueComponent({
	template:
		`<div id="loadingScreen" style="text-align:center" class="active">
		<h1>Loading...</h1>
		<h2>{{state.complete}}/{{state.total}}</h2>
	</div>`
})
class LoadingDlg extends Vue {
	state = loadState;
	startTime: Date;

	static open(): Promise<any> {
		var vm = new LoadingDlg();
		dlg.open(vm);
		vm.startTime = new Date();

		let p = preload().then(() => {
			generateNumberIcons();
			setTimeout(() => {
				vm.close();
			}, 100);
		});

		return p;
	}

	close() {
		dlg.close(this);
	}
}