@VueComponent({
	template: templates.fetch('gameOverDlg')
})
class GameOverDlg extends Vue{
	playerWon:boolean = false;

	mounted(){
		this.$el.scrollTop = 0;
	}

	close(){
		dlg.close(this);
		state.reset();
		newGameDlg.open();
	}
}

var gameOverDlg = {
	open: function(playerWon:boolean){
		var vm = new GameOverDlg();
		vm.playerWon = playerWon;
		dlg.open(vm);
	}
}