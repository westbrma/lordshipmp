@VueComponent({
	template: templates.fetch('newCityDlg')
})
class NewCityDlg extends Vue {
	name: string = null;
	canAfford: boolean = true;

	static open() {
		var vm = new NewCityDlg();
		vm.canAfford = state.playerArmy.canAffordCity;
		dlg.open(vm);
	}

	build() {
		var city = state.playerArmy.buildCity(this.name);
		this.close();
	}

	close() {
		dlg.close(this);
	}
}