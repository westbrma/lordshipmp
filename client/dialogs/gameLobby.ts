@VueComponent({
	template: templates.fetch('gameLobby')
})
class GameLobbyDlg extends Vue {
	message = '';
	state = state;
	users = [];
	game: Game = state.game;
	aiPlayers: number = 3;
	victoryPoints: number = 100;
	mapSizes: IMapSize[] = [
		{ width: 30, height: 15 },
		{ width: 40, height: 20 },
		{ width: 50, height: 30 }];

	mapSize: IMapSize = null;

	static open() {
		var vm = new GameLobbyDlg();
		vm.$watch('state.game.active', () => {
			if (state.game.active) {
				vm.close();
			}
		});

		vm.$watch('state.messages', () => {
			Vue.nextTick(() => {
				var d = $('.messages');
				d.scrollTop(d.prop("scrollHeight"));
			});
		});

		dlg.open(vm);
	}

	created() {
		this.mapSize = this.mapSizes[0];
		app.$on(AppEvent.GameDestroyed, () => {
			this.close();
		});
	}

	get mapSizeDisplay():string{
		return `${this.mapSize.width} x ${this.mapSize.height}`;
	}

	bumpMapSize(){
		let idx= this.mapSizes.indexOf(this.mapSize);
		idx++;
		if(idx >= this.mapSizes.length){
			idx= 0;
		}
		this.mapSize = this.mapSizes[idx];
	}

	bumpAI(){
		this.aiPlayers++;
		if(this.aiPlayers > 5){
			this.aiPlayers = 0;
		}
	}

	bumpVictoryPoints(){
		this.victoryPoints += 50;
		if(this.victoryPoints > 300){
			this.victoryPoints = 100;
		}
	}

	close() {
		dlg.close(this);
	}

	disconnect() {
		api.leaveGame(state.game);
		this.close();
	}

	sendMessage() {
		api.postMessage(this.message);
		this.message = '';
	}

	cycleColor(player: IPlayer) {
		if (this.state.player.id != player.id) {
			return;
		}

		player.color = cycleNext(player.color, colors);
		api.updatePlayer(player);
	}

	startGame() {
		let game:IStartGame = {
			type: MessageType.StartGame,
			aiPlayers: this.aiPlayers,
			mapWidth: this.mapSize.width,
			mapHeight: this.mapSize.height,
			victoryPoints: this.victoryPoints
		}

		api.startGame(game);
	}
}