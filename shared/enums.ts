enum ObjType{
	Recruit,
	Discovery,
	Merchant,
	Chest,
	Army,
	City,
	Animal,
	Creep,
	Expedition,
	Posting
}

enum MessageType {
	// messages sent from client to server
	JoinGame = "JoinGame",
	LeaveGame = "LeaveGame",
	PostMessage = "PostMessage",
	CreateGame = "CreateGame",
	StartGame = "StartGame",
	TakeTurn = "TakeTurn",
	UpdatePlayer = "UpdatePlayer",

	// messages sent from server to client
	UpdateGame = "UpdateGame",
	GameInfo = "GameInfo",
	GameDestroyed = "GameDestroyed",
	GameList = "GameList",
	Error = "Error"
}

enum TileType {
	Grass = 1,
	Mountain = 2,
	Trees = 3,
	Sand = 4,
	Dirt = 5,
	Sheep = 6,
	Village = 7,
	Gold = 8,
	Farm = 9,
	Water = 10
}

enum ProdType{
	Knight='Knight',
	Woodsman = 'Woodsman',
	Castle = 'Castle'
}

enum SkillType{
	Hunting='Hunting',
	Exploring='Exploring',
	Combat='Combat',
	Trading='Trading',
	Travel='Travel',
	Charisma='Charisma',
	Sailing='Sailing',
	Locks='Locks',
	Fishing='Fishing',
	Traps='Traps'
}

enum TaskType{
	Skill = 'Skill',
	Reward = 'Reward',
	Trap = 'Trap',
	Chance = 'Chance',
	Battle = 'Battle',
	Combat = 'Combat',
	Requirement = 'Requirement',
	GoldReq = 'Gold'
}

enum TileVis {
	Hidden = 0,
	Visible = 1,
	Fog = 2
}

enum RewardType {
	Gold = 'Gold',
	Chest = 'Chest',
	Score = 'Score',
	Food = 'Food',
	Unit = 'Unit',
	Item = 'Item'
}

var rewardIcons: { [key: string]: string } = {
	'Gold': 'images/rewards/gold.png',
	'Chest': 'images/rewards/chest.png',
	'Score': 'images/rewards/score.png',
	'Food': 'images/rewards/food.png',
	'Unit': 'images/rewards/unit.png',
	'Item': 'images/cards/princess.png'
}

enum TeamType {
	CPU = "CPU",
	Player = "Player"
}

enum ExpeditionStatus {
	Pending = "Pending",
	Completed = "Completed",
	Failed = "Failed"
}

const CityTurns = 10;
const CityView = 1;
const CityCost: number = 100;
const MaxMove: number = 3;
const ViewDistance:number = 3;

if(typeof exports !== 'undefined'){
	var g:any = global as any;
	g.MessageType = MessageType;
	g.ObjType = ObjType;
	g.TileVis = TileVis;
	g.TaskType = TaskType;
	g.TileType = TileType;
	g.RewardType = RewardType;
	g.rewardIcons = rewardIcons;
	g.TeamType = TeamType;
	g.ExpeditionStatus = ExpeditionStatus;
	g.SkillType = SkillType;
	g.ProdType = ProdType;

	g.CityTurns = CityTurns;
	g.CityView = CityView;
	g.CityCost = CityCost;
	g.MaxMove = MaxMove;
	g.ViewDistance = ViewDistance;
}