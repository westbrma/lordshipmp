interface ISkillDef{
	type:SkillType;
	icon:string;
}

var skillList:ISkillDef[] = [
	{
		type: SkillType.Sailing,
		icon: 'images/skills/sailing.png'
	},
	{
		type: SkillType.Travel,
		icon: 'images/skills/traveling.png'
	},
	{
		type: SkillType.Hunting,
		icon: 'images/skills/hunting.png'
	},
	{
		type: SkillType.Exploring,
		icon: 'images/skills/exploring.png'
	},
	{
		type: SkillType.Combat,
		icon: 'images/skills/combat.png'
	},
	{
		type: SkillType.Trading,
		icon: 'images/skills/trading.png'
	},
	{
		type: SkillType.Charisma,
		icon: 'images/skills/charisma.png'
	},
	{
		type: SkillType.Locks,
		icon: 'images/skills/key.png'
	},
	{
		type: SkillType.Fishing,
		icon: 'images/skills/fishing.png'
	},
	{
		type: SkillType.Traps,
		icon: 'images/skills/trap.png'
	}
];

var skillMap:{[key:string]:ISkillDef} = {};
skillList.forEach(s => {
	skillMap[s.type] = s;
});

if(typeof exports !== 'undefined'){
	var g:any = global as any;
	g.skillList = skillList;
	g.skillMap = skillMap;
}