var prodList:IProdDef[] = [
	{
		name: ProdType.Castle,
		icon: 'images/city.png',
		cost: 20
	},
	{
		name: ProdType.Knight,
		icon: 'images/units/knight.png',
		cost: 20
	},
	{
		name: ProdType.Woodsman,
		icon: 'images/units/woodsman.png',
		cost: 20
	}
];

var prodMap:{[key:string]:IProdDef} = {};
prodList.forEach(s => {
	prodMap[s.name] = s;
});

if(typeof exports !== 'undefined'){
	var g:any = global as any;
	g.prodList = prodList;
	g.prodMap = prodMap;
}