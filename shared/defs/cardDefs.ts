enum ItemType{
	Teleport='Teleport Scroll',
	LuckyDie = 'Lucky Die',
	Shield = 'Shield',
	Princess = 'Princess',
	Relic = 'Relic'
}

var cardList:ICardDef[] = [
	{
		name: ItemType.Teleport,
		icon: 'images/cards/teleport.png',
		info: 'Instantly Teleports expedition to location'
	},
	{
		name: ItemType.LuckyDie,
		icon: 'images/cards/luckyDie.png',
		info: '+25% on all mission chance objectives'
	},
	{
		name: ItemType.Shield,
		icon: 'images/cards/shield.png',
		info: 'Absorbs 5 damage'
	},
	{
		name: ItemType.Princess,
		icon: 'images/cards/princess.png'
	},
	{
		name: ItemType.Relic,
		icon: 'images/cards/relic.png',
		info: 'Doubles income from a merchant mission'
	}
];

var cardMap:{[key:string]:ICardDef} = {};
cardList.forEach(s => {
	cardMap[s.name] = s;
});

var chestList:ICardDef[] = [
	cardMap[ItemType.LuckyDie],
	cardMap[ItemType.Teleport],
	cardMap[ItemType.Shield],
	cardMap[ItemType.Relic]
]

if(typeof exports !== 'undefined'){
	var g:any = global as any;
	g.cardMap = cardMap;
	g.chestList = chestList;
	g.cardList = cardList;
	g.ItemType = ItemType;
}