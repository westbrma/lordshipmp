enum UnitType {
	Knight = 'Knight',
	Rogue = 'Rogue',
	Sailer = 'Sailer',
	Peasant = 'Peasant',
	Scout = 'Scout',
	Diplomat = 'Diplomat',
	Bard = 'Bard',
	Woodsman = 'Woodsman'
}

var unitList: IUnitDef[] = [
	{
		name: UnitType.Knight,
		icon: 'images/units/knight.png',
		skills: { 'Combat': 2, 'Exploring': 1 },
		isUnit: true
	},
	{
		name: UnitType.Rogue,
		icon: 'images/units/thief.png',
		skills: { 'Locks': 2, 'Combat': 1, 'Traps': 2 },
		isUnit: true
	},
	{
		name: UnitType.Sailer,
		icon: 'images/units/pirate.png',
		skills: { 'Fishing': 1, 'Sailing': 2, 'Combat': 1 },
		isUnit: true
	},
	{
		name: UnitType.Bard,
		icon: 'images/units/bard.png',
		skills: { 'Charisma': 2, 'Trading': 1 },
		isUnit: true
	},
	{
		name: UnitType.Woodsman,
		icon: 'images/units/woodsman.png',
		skills: { 'Charisma': 1, 'Hunting': 2, 'Exploring': 2 },
		isUnit: true
	}
];

var unitMap: { [key: string]: IUnitDef } = {};
unitList.forEach(s => {
	for (var prop in s.skills) {
		if (skillMap[prop] == undefined) {
			throw new Error('Unknown Skill: ' + prop);
		}
	}
	unitMap[s.name] = s;
});

if(typeof exports !== 'undefined'){
	var g:any = global as any;
	g.unitList = unitList;
	g.UnitType = UnitType;
	g.unitMap = unitMap;
}