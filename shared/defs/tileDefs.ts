var tileDefs: TileDefs = null;

class TileDefs {
	_defs: { [key: string]: TileDef } = {};
	getDef(type: TileType): TileDef {
		return this._defs[type];
	}

	constructor() {
		tileDefs = this;
		new TileDef(TileType.Grass, "Field", 2, 2, 0, 0, 1, ['grass','grass2']);
		new TileDef(TileType.Gold, "Gold Mine", 0, 0, 0, 5, 3, ['gold','gold_alt']);
		new TileDef(TileType.Mountain, "Mountain", 0, 0, 2, 1, 3, ['rock','rock2']);
		new TileDef(TileType.Trees, "Forest", 2, 1, 1, 0, 2, ['tree','tree2']);
		new TileDef(TileType.Sand, "Sand", 1, 1, 1, 0, 2, ['sand']);
		new TileDef(TileType.Dirt, "Dirt", 1, 0, 1, 0, 1, ['dirt']);
		new TileDef(TileType.Farm, "Farm", 2, 3, 0, 0, 2, ['farm']);
		new TileDef(TileType.Sheep, "Sheep", 0, 4, 0, 0, 2, ['sheep','sheep_alt']);
		new TileDef(TileType.Village, "Village", 10, 0, 0, 0, 2, ['village','village_alt']);
		new TileDef(TileType.Water, "Sea", 0, 1, 1, 0, 1, ['water','water_alt']);
	}
}

class TileDef {
	Name: string;
	Gold: number;
	Population: number;
	Production: number;
	Food: number;
	TravelSpeed: number;
	Type: TileType;
	images: HTMLImageElement[] = [];
	icons: string[]= [];

	constructor(type: TileType, name: string, pop: number, food: number, prod: number, gold: number, travel: number, icons:string[]) {
		this.Type = type;
		this.Name = name;
		this.Population = pop;
		this.Food = food;
		this.Production = prod;
		this.Gold = gold;
		this.TravelSpeed = travel;
		this.icons = icons;
		tileDefs._defs[type] = this;
	}

	getIconUrl(iconName:string){
		return `images/tiles/${iconName}.png`;
	}
}

new TileDefs();

if(typeof exports !== 'undefined'){
	var g:any = global as any;
	g.tileDefs = tileDefs;
	g.TileDef = TileDef;
}