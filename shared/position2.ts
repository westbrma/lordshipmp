class Position2 {
	x: number;
	y: number;

	constructor(x, y) {
		this.x = x;
		this.y = y;
	}

	diff(other: Position2): Position2 {
		return new Position2(this.x - other.x, this.y - other.y);
	}

	length(): number {
		return Math.abs(this.x) + Math.abs(this.y);
	}

	dist(other: Position2): number {
		var diff = this.diff(other);
		return diff.length();
	}
}

if(typeof exports !== 'undefined'){
	var g:any = global as any;
	g.Position2 = Position2;
}