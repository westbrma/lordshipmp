interface IGame {
	id: number;
	active: boolean;
	maxPlayers: number;
	ownerId: number;
	players?: IPlayer[]
}

type SkillLevelMap = { [key: string]: number };

interface IPlayer {
	id: number;
	name: string;
	color: string;
}

interface IBoard {
	width:number;
	height:number;
	tiles:TileType[][];
}

interface ICard {
	id:number;
	name:string;
	onMission:boolean
}

interface IReward {
	type: RewardType;
	amount?: number;
}

interface IObjective {
	type: TaskType;
	skill: SkillType;
	amount: number;
	itemName?:string;
}

interface IMapObject{
	id:number;
	type:ObjType;
	tile:ICoord;
	hp?:number;
	objectives: IObjective[];
	skills: SkillLevelMap;
	reward?:IReward;
	teamId?:number;
	eta?:number;
	travelRemaing?:number;
}

interface IArmy extends IMapObject{
	score:number;
	cards?:ICard[];
	food?:number;
	gold?:number;
}

interface ICity extends IMapObject {
	expPts:number;
	name:string;
	prod:ProdType;
	prodAmount:number;
	production:number;
	gold:number;
	food:number;
	population:number;
}

interface ICoord {
	x:number;
	y:number;
}

interface INewExpedition{
	objectId:number;
	cardIds?:number[];
}

interface ICityUpdate {
	newCity?:boolean;
	cityName?:string;
	cityId?:number;
	expansions?:ICoord[];
	prodType?:ProdType;
}

interface ITakeTurn {
	type: MessageType,
	move?:ICoord,
	cityUpdates:ICityUpdate[];
	newExpeditions:INewExpedition[];
}

interface ITileUpdate {
	x:number;
	y:number;
	cityId:number;
	occupantId:number;
}

interface IPlayerUpdate {
	id:number;
	location:ICoord;
}

interface IMessage {
	id?: string;
	type: MessageType;
	error?: string;
}

interface IStartGame extends IMessage {
	aiPlayers:number;
	mapWidth:number;
	mapHeight:number;
	victoryPoints:number;
}

interface ICreateGameMsg extends IMessage {
	game: IGame;
}

interface IGameListMsg extends IMessage {
	games?: IGame[];
}

interface IGameInfo extends IMessage {
	game:IGame;
}

interface IJoinGameMsg extends IMessage {
	gameId: number;
	player: IPlayer;
	joined?:boolean;
}

interface IPlayerListMsg extends IMessage {
	players: IPlayer[];
}

interface IPostMsg extends IMessage {
	messsage: string;
}

interface ILeaveGame extends IMessage {
	gameId: number;
}

interface ISkillLevel {
	skill: SkillType;
	count: number;
}

interface IVisTiles {
	x:number[];
	y:number[];
}

interface IMyInfo {
	gold:number;
	food:number;
	cards:ICard[];
	informs?:string[];
	visTiles?:IVisTiles;
}

interface IUpdateGame extends IMessage{
	type: MessageType;
	mapObjects:IMapObject[];
	deletedIds?:number[];
	tileUpdates?:ITileUpdate[];
	myInfo:IMyInfo;
	board?:IBoard;
	start?:boolean;
}

interface IUpdatePlayer extends IMessage {
	player: IPlayer;
}

interface IProdDef {
	name:ProdType;
	cost:number;
	icon:string;
}

interface ICardDef{
	name:string;
	icon:string;
	info?:string;
	skills?:SkillLevelMap;
	isUnit?:boolean
}

interface IUnitDef extends ICardDef {

}